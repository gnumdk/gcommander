/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "utils.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

/******************************************************************************************************************************/
/*read config*/
gint read_config(struct GCommander *gcommander)
{
	struct GC_group *gc_grp;
	gint to_return = 0;
	
	if(! g_file_test(g_strconcat(g_get_home_dir(), "/", CFG, NULL), G_FILE_TEST_IS_DIR) ||
	   ! g_file_test(g_strconcat(g_get_home_dir(), "/", GRP_CFG, NULL), G_FILE_TEST_IS_REGULAR))
	{
		if((gc_grp = g_malloc(sizeof(struct GC_group))) == NULL)
			return -1;
		gc_grp->name = g_strdup(DEFAULT_GRP);
		gc_grp->icon = NULL;
		gc_grp->nb_progs = 0;
		gcommander->groups = g_slist_append(gcommander->groups, gc_grp);
	}
	if(read_mime_dd(gcommander) == -1)
		to_return = -1;
	if(read_group_dd(gcommander) == -1)
		to_return = -1;
	return to_return;
}
	
/******************************************************************************************************************************/
/*save config*/
gint save_config(struct GCommander *gcommander)
{
	gchar *cfg_file = g_strconcat(g_get_home_dir(), "/", CFG, NULL); 
	if(! g_file_test(cfg_file, G_FILE_TEST_IS_DIR))
		mkdir(cfg_file, S_IRUSR|S_IWUSR|S_IXUSR);
	if(save_mime_dd(gcommander) == -1)
		return -1;
	if(save_group_dd(gcommander) == -1)
		return -1;
}

/******************************************************************************************************************************/
/*read mime config*/
gint read_mime_dd(struct GCommander *gcommander)
{
	FILE *cfg;
	gint i;
	gchar buff[LG_BUFF];
	gchar *tmp;
	struct GC_mime *gc_mime;	
	
	if((cfg = fopen(g_strconcat(g_get_home_dir(), "/", MIME_CFG, NULL), "r")) == NULL)
		return -1;
	
	/*don't want headers*/
	for(i=0; i<8; i++) fgets(buff, LG_BUFF, cfg);
	
	while(fgets(buff, LG_BUFF, cfg)) /*separation*/
	{
		if((gc_mime = g_malloc(sizeof(struct GC_mime))) == NULL)
			return -1;

		fgets(buff, LG_BUFF, cfg);
		if((gc_mime->name = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
			return;
		sscanf(TO_UTF8(buff), "%[^\n]", gc_mime->name);
		
		fgets(buff, LG_BUFF, cfg);
		
		if(strcmp(buff, "(null)\n"))
		{
			if((tmp = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
				return;
			sscanf(TO_UTF8(buff), "%[^\n]", tmp);
			gc_mime->exts = char2exts(tmp);
		}
		else
			gc_mime->exts = NULL;
		
		fgets(buff, LG_BUFF, cfg);
		if(strcmp(buff, "(null)\n"))
		{
			if((gc_mime->icon = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
				return;
			sscanf(buff, "%[^\n]", gc_mime->icon);
		}
		fgets(buff, LG_BUFF, cfg);
		//if(strcmp(buff, "(null)\n")) //#FIXME
		//{
			if((gc_mime->group = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
				return;
			sscanf(TO_UTF8(buff), "%[^\n]", gc_mime->group);
		//} pretty fix :)
		fgets(buff, LG_BUFF, cfg);
		gc_mime->nb_progs = atoi(buff);
		for(i=0; i<gc_mime->nb_progs; i++)
		{
			fgets(buff, LG_BUFF, cfg);
			sscanf(TO_UTF8(buff), "%[^\n]", gc_mime->progs[i].name);
			fgets(buff, LG_BUFF, cfg);
			sscanf(TO_UTF8(buff), "%[^\n]", gc_mime->progs[i].cmd);
		}
		gcommander->mimes = g_slist_insert_sorted(gcommander->mimes, gc_mime, mime_cmp);
	}
	
	fclose(cfg);
}

/******************************************************************************************************************************/
/*save mime config*/
gint save_mime_dd(struct GCommander *gcommander)
{
	FILE *cfg;
	struct GC_mime *gc_mime;
	gint i, h;
	gchar *ext;
	
	if((cfg = fopen(g_strconcat(g_get_home_dir(), "/", MIME_CFG, NULL), "w")) == NULL)
		return -1;
	
	fprintf(cfg, "//name\n//extensions\n//icon\n//group\n//number of commands\n//command name\n//command\n");
	fprintf(cfg, "//If you edit this file, don't add blank line!\n");
	for(i=0; i<g_slist_length(gcommander->mimes); i++)
	{
		gc_mime = g_slist_nth_data(gcommander->mimes, i);

		fprintf(cfg, "/***************************************************************/\n");
		if(gc_mime->exts)
			ext = FROM_UTF8(exts2char(gc_mime->exts));
		else 
			ext = NULL;
		fprintf(cfg, "%s\n%s\n%s\n%s\n%d\n", FROM_UTF8(gc_mime->name), ext, gc_mime->icon, FROM_UTF8(gc_mime->group), gc_mime->nb_progs);
		for(h=0; h<gc_mime->nb_progs; h++)
			fprintf(cfg, "%s\n%s\n", FROM_UTF8(gc_mime->progs[h].name), FROM_UTF8(gc_mime->progs[h].cmd));
	}
	fclose(cfg);
}

/******************************************************************************************************************************/
/*read group config*/
gint read_group_dd(struct GCommander *gcommander)
{
	FILE *cfg;
	gint i;
	gchar buff[LG_BUFF];
	struct GC_group *gc_grp;
	gboolean found_default = FALSE;
	
	if((cfg = fopen(g_strconcat(g_get_home_dir(), "/", GRP_CFG, NULL), "r")) == NULL)
		return -1;
	
	/*don't want headers*/
	for(i=0; i<6; i++) fgets(buff, LG_BUFF, cfg);
	
	while(fgets(buff, LG_BUFF, cfg)) /*separation*/
	{
		if((gc_grp = g_malloc(sizeof(struct GC_group))) == NULL)
			return -1;

		fgets(buff, LG_BUFF, cfg);
		if((gc_grp->name = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
			return;
		sscanf(TO_UTF8(buff), "%[^\n]", gc_grp->name);
		if(!strcmp(gc_grp->name, DEFAULT_GRP))
			found_default = TRUE;
		fgets(buff, LG_BUFF, cfg);
		if(strcmp(buff, "(null)\n"))
		{
			if((gc_grp->icon = g_malloc(sizeof(gchar) * strlen(buff))) == NULL)
				return;
			sscanf(buff, "%[^\n]", gc_grp->icon);
		}
		else
			gc_grp->icon = NULL;
		fgets(buff, LG_BUFF, cfg);
		gc_grp->nb_progs = atoi(buff);
		for(i=0; i<gc_grp->nb_progs; i++)
		{
			fgets(buff, LG_BUFF, cfg);
			sscanf(TO_UTF8(buff), "%[^\n]", gc_grp->progs[i].name);
			fgets(buff, LG_BUFF, cfg);
			sscanf(TO_UTF8(buff), "%[^\n]", gc_grp->progs[i].cmd);
		}
		gcommander->groups = g_slist_insert_sorted(gcommander->groups, gc_grp, group_cmp);
	}
	if(!found_default)
	{
		if((gc_grp = g_malloc(sizeof(struct GC_group))) == NULL)
			return -1;
		gc_grp->name = g_strdup(DEFAULT_GRP);
		gc_grp->icon = NULL;
		gc_grp->nb_progs = 0;
		gcommander->groups = g_slist_insert_sorted(gcommander->groups, gc_grp, group_cmp);
	}
	fclose(cfg);
}

/******************************************************************************************************************************/
/*save mime config*/
gint save_group_dd(struct GCommander *gcommander)
{
	FILE *cfg;
	struct GC_group *gc_grp;
	gint i, h;
	
	if((cfg = fopen(g_strconcat(g_get_home_dir(), "/", GRP_CFG, NULL), "w")) == NULL)
		return -1;
	
	fprintf(cfg, "//name\n//icon\n//number of commands\n//command name\n//command\n");
	fprintf(cfg, "//If you edit this file, don't add blank line!\n");
	for(i=0; i<g_slist_length(gcommander->groups); i++)
	{
		gc_grp = g_slist_nth_data(gcommander->groups, i);
		if(strcmp(gc_grp->name, DEFAULT_GRP))
		{
			fprintf(cfg, "/***************************************************************/\n");
			fprintf(cfg, "%s\n%s\n%d\n", FROM_UTF8(gc_grp->name), gc_grp->icon, gc_grp->nb_progs);
			for(h=0; h<gc_grp->nb_progs; h++)
				fprintf(cfg, "%s\n%s\n", FROM_UTF8(gc_grp->progs[h].name), FROM_UTF8(gc_grp->progs[h].cmd));
		}
	}
	fclose(cfg);
}

/******************************************************************************************************************************/
/*convert a string in a list of word*/
GSList *char2exts(gchar *buff)
{
	GSList *ext_list = NULL;
	gint i;
	gint h = 0;
	gint len = strlen(buff);
	for(i=0; i<len; i++)
	{
		if(buff[i] == ' ')
		{
			buff[i] = '\0';
			ext_list = g_slist_append(ext_list, g_strdup(buff+h));
			h = i+1;
		}
	}
	/*we put last chars*/
	ext_list = g_slist_append(ext_list, g_strdup(buff+h));
	
	return ext_list;
}

/******************************************************************************************************************************/
/*convert a list of word in string*/
gchar *exts2char(GSList *exts)
{
	gint i;
	gchar *result = NULL;

	if(exts)
	{
		i = g_slist_length(exts);
		while(i)
		{
			if(result)
			{
				if((i-1))
					result = g_strconcat(result, g_slist_nth_data(exts, i-1), " ", NULL);
				else
					result = g_strconcat(result, g_slist_nth_data(exts, i-1), NULL);
			}
			else
			{
				result = g_slist_nth_data(exts, i-1);
				if((i-1))
					result = g_strconcat(result, " ", NULL);
			}
			i--;
		}
	}

	return result;
}
