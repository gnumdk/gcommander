/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "list.h"
#include "path_manager.h"
#include "mime_ui.h"

/******************************************************************************************************************************/
/*create file list view*/
void list_view_new(struct GCommander *gcommander)
{
	create_file_list(gcommander);
	gcommander->view = gtk_tree_view_new_with_model(gcommander->model);
	add_columns_set_callbacks(gcommander);
}

/******************************************************************************************************************************/
/*update file list view*/
void update_list_view(struct GCommander *gcommander)
{
	set_cursor_busy(gcommander->main_window);
	gtk_list_store_clear(GTK_LIST_STORE(gcommander->model));
	create_file_list(gcommander);
	set_statusbar(gcommander);;
	gtk_entry_set_text(GTK_ENTRY(gcommander->location), TO_UTF8(gcommander->current_path));
	set_cursor_normal(gcommander->main_window);
}

/******************************************************************************************************************************/
/*remove file list view*/
void remove_list_view(struct GCommander *gcommander)
{
	gtk_list_store_clear(GTK_LIST_STORE(gcommander->model));
}

/******************************************************************************************************************************/
/*add columns to treeview and set callbacks*/
void add_columns_set_callbacks(struct GCommander *gcommander)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(gcommander->view));

	renderer = gtk_cell_renderer_pixbuf_new ();

	column = gtk_tree_view_column_new_with_attributes("", renderer, "pixbuf", 
								FILE_ICON, NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(gcommander->view), column);
	
	renderer = gtk_cell_renderer_text_new ();

	column = gtk_tree_view_column_new_with_attributes("File", renderer, "text", 
								FILE_NAME, NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(gcommander->view), column);
/*
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes("Type",
													  renderer,
													  "text",
													  FILE_TYPE,
													  NULL);
	
	gtk_tree_view_append_column(gcommander->view, column);
	*/
	gtk_tree_selection_set_mode(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)), GTK_SELECTION_MULTIPLE);

	g_signal_connect(G_OBJECT(gcommander->view), "event_after",
					 G_CALLBACK(list_clicked), gcommander);
	g_signal_connect(G_OBJECT(gcommander->view), "button_press_event",
					 G_CALLBACK(button_press_callback), gcommander);

}

/******************************************************************************************************************************/
/*create a list of file*/
void create_file_list(struct GCommander *gcommander)
{
	GtkTreeIter iter;
	GSList *files = NULL;
	static GdkPixbuf *icon_up = NULL;
	struct GC_file *file;
	gint i = 0;

	if(! icon_up)
		icon_up = gdk_pixbuf_new_from_file(DEFAULT_UP_ICON, NULL);

	/* create list store */
	if(!gcommander->model)
		gcommander->model = GTK_TREE_MODEL(gtk_list_store_new(	NB_COLUMNS,
									GDK_TYPE_PIXBUF,
									G_TYPE_STRING,
									G_TYPE_INT));

	/*add .. if not root directory*/
	if(strcmp(gcommander->current_path, "/"))
	{
		gtk_list_store_append(GTK_LIST_STORE(gcommander->model), &iter);

		gtk_list_store_set(GTK_LIST_STORE(	gcommander->model), &iter,
							FILE_ICON, icon_up,
							FILE_NAME, "..",
							FILE_TYPE, GC_DIR,
							-1);
	}
	files = list_path(gcommander->current_path, DIR_FIRST, gcommander);
	files = g_slist_sort(files, file_cmp);
	g_slist_foreach(files, list_append_foreach, gcommander->model);
	g_slist_free(files);

}

/******************************************************************************************************************************/
/*use by create_file_list to add values to list*/
void list_append_foreach(gpointer data, gpointer store)
{
	GtkTreeIter iter;
	struct GC_file *file = data;
	gchar *type;	

	gtk_list_store_append(store, &iter);

	gtk_list_store_set(store, &iter,
					   FILE_ICON, file->icon,
					   FILE_NAME, file->name,
					   FILE_TYPE, file->type,
					   -1);
	g_free(file->name);
	g_free(file);
}

/******************************************************************************************************************************/
/*fucking nautilus function, free software powa!*/
void tree_view_get_selection_foreach_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{	
	GSList **list;
	struct GC_file *file;
	gchar *type;
	
	if((file = g_malloc(sizeof(struct GC_file *))) == NULL)
		return;

	list = data;
	gtk_tree_model_get(model, iter,
					   FILE_NAME, &file->name,
					   FILE_TYPE, &file->type,	
					   -1);

	(*list) = g_slist_prepend((*list), file);
}

/******************************************************************************************************************************/
/*get the first selected file*/
struct GC_file *selected_one(GtkWidget *list)
{
	struct GC_file *selected = NULL;
	GSList *selected_list = NULL;	

	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(list)),
						tree_view_get_selection_foreach_func, &selected_list);
	if(selected_list)
		selected = g_slist_nth_data(selected_list, 0);

	g_slist_free(selected_list);
	
	return selected;
}

/******************************************************************************************************************************/
/*return true if we have only one element selected*/
gboolean one_element_selected(GtkWidget *list)
{
	struct GC_file *selected = NULL;
	GSList *selected_list = NULL;	
	gboolean only_one=FALSE;
	
	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(list)),
						tree_view_get_selection_foreach_func, &selected_list);
	if(selected_list)
		only_one = ( g_slist_length(selected_list) == 1 );

	g_slist_free(selected_list);
	
	return only_one;
}

/******************************************************************************************************************************/
/*go to directory path*/
void list_clicked(GtkWidget *tree, GdkEventButton *event, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
		
	if(event->button == 1 && (event->type == GDK_BUTTON_RELEASE && !gcommander->double_click ||
		event->type == GDK_2BUTTON_PRESS && gcommander->double_click))
		file_do(tree, gcommander);
	else if(event->button == 3 && event->type == GDK_BUTTON_RELEASE)
		view_popup(gcommander);
		
}

/******************************************************************************************************************************/
/*perform action on selected list file*/
void file_do(GtkWidget *widget, gpointer data)
{
	struct GC_file *selected_file;
	struct GCommander *gcommander = (struct GCommander *) data;
	struct GC_mime *gc_mime;	
	gchar *new_path;
	gchar *old_path;	

	if(selected_file = selected_one(gcommander->view))
	{
		if(selected_file->type == GC_DIR || selected_file->type == GC_LINK_DIR)
		{
			old_path = g_strdup(gcommander->current_path);
			new_path = g_filename_from_utf8(selected_file->name, -1, NULL, NULL, NULL);
			if(chdir(new_path) != -1)
			{
				getcwd(gcommander->current_path, LG_PATH);
				g_slist_free(gcommander->forward);
				gcommander->forward = NULL;
				gcommander->backward = g_slist_prepend(gcommander->backward, old_path);
				update_list_view(gcommander);
			}
			g_free(new_path);
		}
		else
		{
			if(! gcommander->mime_is_show)
				gc_exec(gcommander, selected_file->name);
		}
	}
}

/******************************************************************************************************************************/
/*select all files*/
void select_all(GtkWidget *widget, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;

	gtk_tree_selection_select_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)));

}

/******************************************************************************************************************************/
/*disable default code
taken from nautilus, thanx :)*/
gboolean button_press_callback(GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
	GtkTreePath *path;
	gboolean result;

	if (event->window != gtk_tree_view_get_bin_window(GTK_TREE_VIEW(gcommander->view)))
		return FALSE;
	
	result = FALSE;

	if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(gcommander->view), event->x, event->y, &path, NULL, NULL, NULL)) 
	{
		if (event->button == 3 && 
			gtk_tree_selection_path_is_selected(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)), path))
		{
			/* Don't let the default code run because if multiple rows
			   are selected it will unselect all but one row; but we
			   want the right click menu to apply to everything that's
			   currently selected. */
			result = TRUE;
		}

		gtk_tree_path_free (path);
	}
	else
	{
		/* Deselect if people click outside any row. It's OK to
		   let default code run; it won't reselect anything. */
		gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)));
	}

	return result;
	
}
