/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "main_interface.h"
#include "path_manager.h"
#include "gcommander.h"
#include "action_dg.h"
#include <string.h>
 
#define PACKAGE    "gcommander"
#define LOCALDIR "/usr/share/locale"

/******************************************************************************************************************************/
/*check if we have file to delete, then deletes one*/
void check_to_delete(struct GCommander *gcommander, gboolean *lock_loop)
{
	static GSList *dir_list = NULL;
	struct GC_file *file; /*one file*/
	gchar *path_to_file;

	/*if we have file to delete, we do it*/
	if(gcommander->edit.to_delete)
	{puts("plop");
		*lock_loop = FALSE;
		file = g_slist_nth_data(gcommander->edit.to_delete, 0);
		path_to_file = g_strconcat(file->path, "/", file->name, NULL);
		
		if(g_file_test(path_to_file, G_FILE_TEST_IS_DIR) && !g_file_test(path_to_file, G_FILE_TEST_IS_SYMLINK))
		{
			dir_list = g_slist_prepend(dir_list, FROM_UTF8(path_to_file));
		}
		else
		{
			
			remove(FROM_UTF8(path_to_file));
			g_free(path_to_file);
		}
		gcommander->edit.to_delete = g_slist_remove(gcommander->edit.to_delete, file);
	
		if(g_slist_length(gcommander->edit.to_delete) == 0)
		{
			puts("FIN DELETE");
			g_free(gcommander->edit.to_delete);
			gcommander->edit.to_delete = NULL;
			g_slist_foreach(dir_list, (GFunc) rmdir, NULL); /*we remove dead directories*/
			g_slist_free(dir_list);
			dir_list = NULL;
			update_list_view(gcommander);
			*lock_loop = TRUE;
			if(gcommander->edit.lock_paste)
				gcommander->edit.lock_paste = FALSE;
			else
				gcommander->edit.action = FALSE;
		}
	}
}
/******************************************************************************************************************************/
/*check if we have file to paste, then paste one*/
void check_to_paste(struct GCommander *gcommander, gboolean *lock_loop)
{
	static GSList *dir_list = NULL;
	static struct GC_paste *paste_files = NULL; 
	static struct GC_file *file = NULL;
	static gchar *path=NULL;
	gchar *path_to_file;
	gchar *formated_file_name;
	static gchar *dest, *src;
	static gint have_to_finish = 0;
puts("FCT");
	/*if a file isn't totally pasted*/
	if(have_to_finish == 2)
	{puts("kppoppp");
		have_to_finish = copy(NULL, NULL);puts("..........");
		if(have_to_finish == 0)
		{puts("coin?");
			if(paste_files->cut == TRUE)
			{
				remove(src);
			}
			g_free(src);
			
			paste_files->files = g_slist_remove(paste_files->files, file);
			g_free(file); /*FIXME*/
		}	
	}
	else if(have_to_finish == -1)
		getchar();//puts("FUCKS");
	/*if we have file to delete, we do it*/
	else if(gcommander->edit.to_paste)
	{
		*lock_loop = FALSE; /*we unlock gtk loop*/
		
		/*if we haven't any files group to paste*/
		if(!paste_files) /*we paste first files group*/
			paste_files = g_slist_nth_data(gcommander->edit.to_paste, 0);

		/*we get first file*/
		file = g_slist_nth_data(paste_files->files, 0);
		
		/*paste in selected dir, in current if no dir selected*/
		if(gcommander->edit.selected)
			formated_file_name = g_strconcat(gcommander->edit.selected, "/", file->name, NULL);
		else
			formated_file_name = g_strdup(file->name);
		
		dest = g_strconcat(paste_files->to_path, "/", formated_file_name, NULL); 
		src = g_strconcat(file->path, "/", file->name, NULL); 
printf("<<%s->%s>>\n", src, dest);
		/*if a directory, we create it #FIXME: if a symbolic link....*/
		if(g_file_test(src, G_FILE_TEST_IS_DIR) && !g_file_test(src, G_FILE_TEST_IS_SYMLINK))
		{
			mkdir(dest, S_IRUSR|S_IWUSR|S_IXUSR);
			if(gcommander->edit.cut)
			{
				dir_list = g_slist_append(dir_list, src);
			}
			g_free(src);
			
			paste_files->files = g_slist_remove(paste_files->files, file);
			g_free(file); /*FIXME*/
		}
		else
		{
			have_to_finish = copy(dest, src);
			if(have_to_finish == 0)
			{
				if(paste_files->cut == TRUE)
				{
					remove(src);
				}
				g_free(src);
			
				paste_files->files = g_slist_remove(paste_files->files, file);
				g_free(file); /*FIXME*/
			}	
		}
	
		g_free(dest);
		g_free(formated_file_name);
	}
	
	if(paste_files)
		if(g_slist_length(paste_files->files) == 0)
		{	puts("00000000000000000000000000000000000");
			if(paste_files->cut == TRUE)
			{
				g_slist_foreach(dir_list, (GFunc) rmdir, NULL); /*we remove dead directories*/
				g_slist_free(dir_list);
				dir_list = NULL;
			}
			gcommander->edit.to_paste = g_slist_remove(gcommander->edit.to_paste, paste_files);
			g_free(paste_files); /*FIXME*/
			paste_files = NULL;
			if(!gcommander->edit.to_paste)
			{
				*lock_loop = TRUE;
				gcommander->edit.action = FALSE;
				update_list_view(gcommander);
			}
		}	
}




/******************************************************************************************************************************/
/******************************************************************************************************************************/
int main(int argc, char **argv)
{	
	struct GCommander *gcommander;
	gboolean lock_loop = TRUE;
	gint copy_result;
	
	bindtextdomain (PACKAGE, LOCALEDIR);
	textdomain (PACKAGE);
	
	gnome_init(APP_NAME, VERSION, argc, argv);
	
	gcommander = main_window_new();
	
	while(! gcommander->quit)
	{
		
		printf("main: %d\n", gcommander->edit.lock_paste);
		if(gcommander->edit.action)
		{
			check_to_delete(gcommander, &lock_loop);
			if(! gcommander->edit.lock_paste)
				check_to_paste(gcommander, &lock_loop);
		}
		gtk_main_iteration_do(lock_loop);
	}

	return 0;
	
}
