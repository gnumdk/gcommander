/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "history.h"

/*move in backward directory*/
void history_go_back(GtkWidget *widget, struct GCommander *gcommander)
{
	history_go(gcommander, GO_BACK);
}

/*move in forward directory*/
void history_go_fwd(GtkWidget *widget, struct GCommander *gcommander)
{
	history_go(gcommander, GO_FWD);
}

/*move in .. directory*/
void history_go_up(GtkWidget *widget, struct GCommander *gcommander)
{
	if(! (gcommander->current_path[0] == '/' && gcommander->current_path[1] == '\0'))
		history_go(gcommander, GO_UP);
}

/*move in home directory*/
void go_home(GtkWidget *widget, struct GCommander *gcommander)
{
	history_go(gcommander, GO_HOME);
}

/*move in history with action*/
void history_go(struct GCommander *gcommander, enum history_actions action)
{
	gchar *going_dir = NULL;
	
	if(action == GO_BACK)
	{
		if(gcommander->backward)
		{
			going_dir = g_slist_nth_data(gcommander->backward, 0);
			gcommander->backward = g_slist_remove(gcommander->backward, going_dir);
			if(g_slist_length(gcommander->backward) == 0) gcommander->backward = NULL;
			/*add item to forward list*/
			gcommander->forward = g_slist_prepend(gcommander->forward, g_strdup(gcommander->current_path));
		}
	}
	else if(action == GO_FWD)
	{
		if(gcommander->forward)
		{
			going_dir = g_slist_nth_data(gcommander->forward, 0);
			gcommander->forward = g_slist_remove(gcommander->forward, going_dir);
			if(g_slist_length(gcommander->forward) == 0) gcommander->forward = NULL;
			/*add item to backward list*/
			gcommander->backward = g_slist_prepend(gcommander->backward, g_strdup(gcommander->current_path));
		}
	}
	else
	{
		if(action == GO_UP)
			going_dir = g_strdup("..");
		else
			going_dir = getenv("HOME");
			
		gcommander->backward = g_slist_prepend(gcommander->backward, g_strdup(gcommander->current_path));
		/*clear forward list*/
		g_slist_free(gcommander->forward);
		gcommander->forward = NULL;
	}
	if(going_dir)
	{
		if(chdir(going_dir) == -1)
			return;
		getcwd(gcommander->current_path, LG_PATH);
		g_free(going_dir);
		update_list_view(gcommander);
	}
}
