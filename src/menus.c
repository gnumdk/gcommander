/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "menus.h"
#include "callbacks.h"
#include "file_manager.h"
#include "path_manager.h"
#include "list.h"
#include "mime_ui.h"
#include "mime_exec.h"
#include "group.h"
#include "main_interface.h"

/******************************************************************************************************************************/
/*create menus*/
void menus_new(struct GCommander *gcommander)
{
	gchar *click_policy;
	
	if(gcommander->double_click)
		click_policy = g_strdup(_("Simple click mode"));
	else
		click_policy = g_strdup(_("Double click mode"));
	
	{
		GnomeUIInfo file_menu[] =
		{
			GNOMEUIINFO_MENU_NEW_ITEM(_("New window"), _("Create a new window"), NULL, NULL),
			GNOMEUIINFO_ITEM_STOCK_DATA(_("Edit groups"), _("Groups properties"), group_window_new, gcommander, GTK_STOCK_CONVERT),
			GNOMEUIINFO_ITEM_STOCK_DATA(_("Edit mimes"), _("Mimes properties"), mime_window_new, gcommander, GTK_STOCK_EXECUTE),
			GNOMEUIINFO_MENU_EXIT_ITEM(gcommander_quit, gcommander),
			GNOMEUIINFO_END
		};
	
		GnomeUIInfo edit_menu[] =
		{
			GNOMEUIINFO_MENU_COPY_ITEM(clipboard_copy, gcommander),
			GNOMEUIINFO_MENU_CUT_ITEM(clipboard_cut, gcommander),
			GNOMEUIINFO_MENU_PASTE_ITEM(action_dg_paste_menu_callback, gcommander),
			GNOMEUIINFO_ITEM_STOCK_DATA(_("Delete"), _("Delete selected files"), action_dg_delete_menu_callback, gcommander, GTK_STOCK_DELETE),
			GNOMEUIINFO_MENU_SELECT_ALL_ITEM(select_all, gcommander),
			GNOMEUIINFO_END
		};
	
		GnomeUIInfo view_menu[] =
		{
			GNOMEUIINFO_TOGGLEITEM_DATA(_("Show hiden files"), NULL, show_hiden_files, gcommander, NULL),
			GNOMEUIINFO_END
		};	
		
		GnomeUIInfo options_menu[] =
		{
			GNOMEUIINFO_TOGGLEITEM_DATA(_(click_policy), NULL, ch_click_policy, gcommander, NULL),
			GNOMEUIINFO_END
		};	
	
		GnomeUIInfo help_menu[] =
		{
			GNOMEUIINFO_HELP ("grecord"),
			GNOMEUIINFO_MENU_ABOUT_ITEM(about, NULL),			
			GNOMEUIINFO_END
		};
		GnomeUIInfo menus[] =
		{
			GNOMEUIINFO_MENU_FILE_TREE(file_menu),
			GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
			GNOMEUIINFO_MENU_VIEW_TREE(view_menu),
			GNOMEUIINFO_MENU_SETTINGS_TREE(options_menu),
			GNOMEUIINFO_MENU_HELP_TREE(help_menu),
			GNOMEUIINFO_END
		};

		gnome_app_create_menus(GNOME_APP(gcommander->main_window), menus);
	}
}
