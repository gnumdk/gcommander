/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "file_manager.h"
#include "path_manager.h"
#include "list.h"
#include <dirent.h>
#include <fcntl.h>
#include <string.h>


/******************************************************************************************************************************/
/*Add an selected item to data list*/
void get_selection_foreach(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{	
	GSList **list;
	struct GC_file *file;
	struct GC_file *file_cp;
	gchar *type;

	if((file_cp = g_malloc(sizeof(struct GC_file *))) == NULL)
		return;
	if((file = g_malloc(sizeof(struct GC_file *))) == NULL)
		return;
	
	list = data;

	gtk_tree_model_get(model, iter,
					   FILE_NAME, &file->name,
					   FILE_TYPE, &file->type,	
					   -1);

	if(file->name[0] == '.' && file->name[1] == '.' && file->name[2] == '\0')
	{
		g_free(file_cp);
	}
	else
	{
		/*we use a copy because after dir change, data will be lose*/
		file_cp->name = g_strdup(file->name);
		file_cp->type = file->type;
		(*list) = g_slist_prepend((*list), file_cp);
	}

}

/******************************************************************************************************************************/
/*copy selected files*/
void clipboard_copy(GtkWidget *widget, struct GCommander *gcommander)
{
	gint i;

	g_slist_free(gcommander->edit.clipboard);
	gcommander->edit.clipboard = NULL;
	
	/*put files in edit list*/
	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)),
									get_selection_foreach, 
									&gcommander->edit.clipboard);

	/*don't cut and save copy path*/
	gcommander->edit.cut = FALSE;
	gcommander->edit.from_path = g_strdup(gcommander->current_path); //FIXME : liberation de from_path
}
	
/******************************************************************************************************************************/
/*cut selected files*/
void clipboard_cut(GtkWidget *widget, struct GCommander *gcommander)
{
	gint i;	

	g_slist_free(gcommander->edit.clipboard);
	gcommander->edit.clipboard = NULL;
	
	/*put files in edit list*/
	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)),
									get_selection_foreach, 
									&gcommander->edit.clipboard);

	/*cut and save copy path*/
	gcommander->edit.cut = TRUE;
	gcommander->edit.from_path = g_strdup(gcommander->current_path);
}

/******************************************************************************************************************************/
/*Remove toggled files*/
void action_start_remove(GtkWidget *widget, gpointer data)
{
	struct GCaction_dg *action_dg = (struct GCaction_dg *) data;
	struct GC_file *file; /*one file*/
	GtkTreeModel *model;	
	GtkTreePath *path;
	GtkTreeIter iter;
	gchar *to_delete;
	gchar *path_to_file;
	gboolean toggle;
	gint i;
	
	/*we have to delete only selected files*/
	if(action_dg->view)
	{
		model = gtk_tree_view_get_model(GTK_TREE_VIEW(action_dg->view));	
		path = gtk_tree_path_new_first();
		while(gtk_tree_model_get_iter(model, &iter, path))
		{
			gtk_tree_model_get(model, &iter,
							   Q_CHECK, &toggle,
							   Q_NAME, &to_delete,	
							   -1);
			
			if(!toggle) /*if no selected, we remove from files list*/
			{puts("debut");
				action_dg->delete_files = remove_from_list(action_dg->delete_files, to_delete, FALSE);
			}puts("fin");
			gtk_tree_path_next(path);
		}
		gtk_tree_path_free(path);
	}

	if(action_dg->delete_files)
	{
		action_dg->gcommander->edit.to_delete = g_slist_concat(action_dg->gcommander->edit.to_delete, action_dg->delete_files);
		action_dg->gcommander->edit.action = TRUE;
	}

	gtk_widget_hide(action_dg->window);
	g_free(action_dg);
}

/******************************************************************************************************************************/
/*paste copied/cuted files
FIXME : PRESERVE LINKS
*/
void action_start_paste(GtkWidget *widget, gpointer data)
{
	struct GCaction_dg *action_dg = (struct GCaction_dg *) data;
	struct GC_file *to_delete_file;
	GSList *to_delete = NULL;
	GtkTreeModel *model;
	GtkTreePath *path;
	GtkTreeIter iter;

	/*if we have file to paste*/
	if(action_dg->gcommander->edit.clipboard)
	{	
		/*if they're are selected files to remove before*/
		if(action_dg->window && action_dg->view)
		{
			gchar *to_remove;
			gboolean toggle;	
			
			model = gtk_tree_view_get_model(GTK_TREE_VIEW(action_dg->view));	
			path = gtk_tree_path_new_first();
			/*get files*/
			while(gtk_tree_model_get_iter(model, &iter, path))
			{
				gtk_tree_model_get(model, &iter,
								   Q_CHECK, &toggle,
								   Q_NAME, &to_remove,	
								   -1);
				if(toggle)
				{
					if((to_delete_file = g_malloc(sizeof(struct GC_file))) == NULL)
						fprintf(stderr, "Allocation error\n");
					else
					{	puts("1");
						to_delete_file->name = FROM_UTF8(to_remove);
						puts("2");
						to_delete_file->path = g_strdup(action_dg->paste_files->to_path);
						puts("3");
						to_delete = g_slist_prepend(to_delete, to_delete_file);
						puts("4");
					}
				}
				else
				{
					action_dg->paste_files->files = remove_from_list(action_dg->paste_files->files, FROM_UTF8(g_basename(to_remove)), TRUE);puts("5");}
				gtk_tree_path_next(path);
				g_free(to_remove);
			}	
			action_dg->gcommander->edit.to_delete = g_slist_concat(action_dg->gcommander->edit.to_delete, to_delete);
			if(! action_dg->gcommander->edit.lock_paste)
				action_dg->gcommander->edit.lock_paste = TRUE;
			gtk_tree_path_free(path);
			gtk_widget_hide(action_dg->window);
		}
		else if(action_dg->window) /*using quiet widget, so we remove all files we have in current dir*/
		{puts("nous y somme");
			struct GC_file *file;
			gint i;
			
			for(i=0; i<g_slist_length(action_dg->paste_files->files); i++)
			{
				file = g_slist_nth_data(action_dg->paste_files->files, i);
				
				if((to_delete_file = g_malloc(sizeof(struct GC_file))) == NULL)
					fprintf(stderr, "Allocation error\n");
				else
				{
					to_delete_file->name = g_strdup(file->name);
					to_delete_file->path = file->path;
					to_delete = g_slist_prepend(to_delete, to_delete_file);
				}
			}
			action_dg->gcommander->edit.to_delete = g_slist_concat(action_dg->gcommander->edit.to_delete, to_delete);
			if(! action_dg->gcommander->edit.lock_paste)
				action_dg->gcommander->edit.lock_paste = TRUE;
			gtk_widget_hide(action_dg->window);
		}
		if(action_dg->paste_files) //FIXME: should work without
		{
			puts("coin");
			action_dg->gcommander->edit.to_paste = g_slist_append(action_dg->gcommander->edit.to_paste, action_dg->paste_files);
			action_dg->gcommander->edit.clipboard = NULL;
			action_dg->gcommander->edit.action = TRUE;
			puts("argh");
		}
	}
	g_free(action_dg);
	
}

/******************************************************************************************************************************/
/*return recursive file list*/
GSList *get_rec_files(struct GC_file *dir, gchar *path)
{
	GSList *files_list = NULL; /*recursive files list*/
	GSList *dirs_list = NULL; /*to be explored dirs list*/
	DIR *list_dir; 
	gchar *wk_dir; /*working directory*/
	gchar *wk_file; /*working file*/
	struct dirent *dir_ent;
	struct stat stat_file;
	struct GC_file *file; /*one file*/
	gint lg_current = strlen(path)+1; /*use to have file path less 'path'*/

	/*GC_file allocation*/
	if((file = g_malloc(sizeof(struct GC_file))) == NULL)
		return;
	file->name = FROM_UTF8(dir->name);
	file->type = GC_DIR;
	file->path = path;
	dirs_list = g_slist_prepend(dirs_list, g_strconcat(path, "/", file->name, NULL));
	/*add root dir to lists*/
	files_list = g_slist_prepend(files_list, file);

	/*while we have dirs to explor*/
	while(g_slist_length(dirs_list))
	{
		/*we take first element*/
		wk_dir = g_slist_nth_data(dirs_list, 0);
		if((list_dir = opendir(wk_dir)) != NULL)
		{
			/*reading directory*/
			while((dir_ent = readdir(list_dir)) != NULL)
			{
				/*don't want ".." && "."*/
				if(!(dir_ent->d_name[0] == '.' && (dir_ent->d_name[1] == '\0' || (dir_ent->d_name[1] == '.' && dir_ent->d_name[2] == '\0'))))
				{

					/*path to current file*/
					wk_file = g_strconcat(wk_dir, "/", dir_ent->d_name, NULL);

					if(stat(wk_file, &stat_file) != -1)
					{
						/*GC_file construction*/
						if((file = g_malloc(sizeof(struct GC_file))) == NULL)
							return NULL;
						
						file->name = g_strdup(wk_file+lg_current);
						file->path = path;
						/*FIXME : only glib functions will be used in future release*/
						/*if file is a directory*/
						if(S_ISDIR(stat_file.st_mode) && !g_file_test(wk_file, G_FILE_TEST_IS_SYMLINK))
						{
							file->type = GC_DIR;
							/*add it to dirs_list and files_list, will be explored next*/
							dirs_list = g_slist_prepend(dirs_list, wk_file);
							files_list = g_slist_append(files_list, file);
						}
						else
						{
							/*add it to files_list*/
							file->type = GC_FILE;
							files_list = g_slist_append(files_list, file);
						}
					}
					else /*broken link*/
					{
						/*GC_file construction*/
						if((file = g_malloc(sizeof(struct GC_file))) == NULL)
							return NULL;
						
						file->name = g_strdup(wk_file+lg_current);
						file->path = path;
						file->type = GC_LINK_BROKEN;
						files_list = g_slist_append(files_list, file);
					}
				}
			}
			/*remove explored dir*/
			dirs_list = g_slist_remove(dirs_list, wk_dir);
			closedir(list_dir);
		}
		else /*we don't have access, so we remove dir from list*/
		{	
			gint i;
			gboolean found = FALSE;

			for(i=0; i<g_slist_length(files_list) && !found; i++)
			{
				file = g_slist_nth_data(files_list, i);
				if(!strcmp(file->name, wk_dir+lg_current))
				{
					files_list = g_slist_remove(files_list, file);
					found = TRUE;
				}
			}
		}
		dirs_list = g_slist_remove(dirs_list, wk_dir);
		g_free(wk_dir);
	}

	return files_list;
}

/******************************************************************************************************************************/
/*copy dest from src
return -1 if failed, 2 if not finished, 0 else*/
int copy(gchar *dest, gchar *src)
{
	gchar buff[LG_BUFF];
	int nb_read;
	static gint open_src = 0;
	static gint open_dest = 0;
	gint result = 0;

	if(!open_src)
		if((open_src = open(src, O_RDONLY)) == -1)
		{
			puts("ici?");
			return -1;
		}

	if(!open_dest)	
		if((open_dest = open(dest, O_WRONLY|O_CREAT|O_EXCL, S_IRUSR|S_IWUSR)) == -1)
		{
			puts("ou bien la?");
			system("ls /home/gnumdk/tmp/32gvu_files/Projects/Gcommander-0.1/bordel/intl");
			getchar();
			close(open_src);
			return -1;
		}

	nb_read = read(open_src, buff, sizeof(buff));
		
	if(nb_read>0)
	{
		if(write(open_dest, buff, nb_read) != nb_read)
		{
			close(open_src);
			close(open_dest);
			open_src = 0;
			open_dest = 0;
			puts("LA!");
			return -1;
		}
		result = 2;
	}
	else
	{
		close(open_src);
		close(open_dest);	
		open_src = 0;
		open_dest = 0;
	}

	return result;	
}
