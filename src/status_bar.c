/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "status_bar.h"

/*create status bar*/
void statusbar_new(struct GCommander *gcommander)
{
	gcommander->gc_statusbar.status_bar = gtk_statusbar_new();	
	gcommander->gc_statusbar.context_id = gtk_statusbar_get_context_id(GTK_STATUSBAR(gcommander->gc_statusbar.status_bar), "number");
	gnome_app_set_statusbar(GNOME_APP(gcommander->main_window), GTK_WIDGET(gcommander->gc_statusbar.status_bar));
	gtk_widget_show(GTK_WIDGET(gcommander->gc_statusbar.status_bar));
}

/*set status bar label*/
void set_statusbar(struct GCommander *gcommander)
{
	gchar msg[LG_BUFF];

	sprintf(msg, "Number of directories : %d. Number of files : %d.",
			gcommander->gc_statusbar.nb_dirs, gcommander->gc_statusbar.nb_files);
	gdk_threads_enter();
	gtk_statusbar_pop(GTK_STATUSBAR(gcommander->gc_statusbar.status_bar), gcommander->gc_statusbar.context_id);
	gtk_statusbar_push(GTK_STATUSBAR(gcommander->gc_statusbar.status_bar), gcommander->gc_statusbar.context_id, msg);
	gdk_threads_leave();
}
