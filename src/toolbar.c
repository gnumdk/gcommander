/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "toolbar.h" 	
#include "history.h"	
#include "file_manager.h"
#include "callbacks.h"

/******************************************************************************************************************************/
/*backward button creation*/
void create_back(GtkWidget *toolbar, struct GCommander *gcommander)
{
	GtkWidget *back_button;
	
	back_button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_GO_BACK, _("Go backward"),
						"private", (GtkSignalFunc) history_go_back, gcommander, -1);
	gtk_widget_show(back_button);
}

/******************************************************************************************************************************/
/*forward button creation*/
void create_fwd(GtkWidget *toolbar, struct GCommander *gcommander)
{
	GtkWidget *fwd_button;
	
	fwd_button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_GO_FORWARD, _("Go forward"),
						"private", (GtkSignalFunc) history_go_fwd, gcommander, -1);
	gtk_widget_show(fwd_button);
}

/******************************************************************************************************************************/
/*backward button creation*/
void create_up(GtkWidget *toolbar, struct GCommander *gcommander)
{
	GtkWidget *up_button;
	
	up_button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_GO_UP, _("Go up"),
						"private", (GtkSignalFunc) history_go_up, gcommander, -1);
	gtk_widget_show(up_button);	
}

/******************************************************************************************************************************/
/*reload button creation*/
void create_reload(GtkWidget *toolbar, struct GCommander *gcommander)
{
	GtkWidget *reload_button;
	
	reload_button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_REFRESH, _("Reload current directory"),
						"private", (GtkSignalFunc) reload, gcommander, -1);
	gtk_widget_show(reload_button);
	
}

/******************************************************************************************************************************/
/*home button creation*/
void create_home(GtkWidget *toolbar, struct GCommander *gcommander)
{
	GtkWidget *home_button;
	
	home_button = gtk_toolbar_insert_stock(GTK_TOOLBAR(toolbar), GTK_STOCK_HOME, _("Go home"),
						"private", (GtkSignalFunc) go_home, gcommander, -1);
	gtk_widget_show(home_button);
	
}

/******************************************************************************************************************************/
/*toolbar creation*/
void toolbar_new(struct GCommander *gcommander)
{
	GtkWidget *toolbar;
	//GtkWidget *vbox;
	//GtkWidget *preview = gtk_image_new_from_file(ICON_APP);
	
	toolbar = gtk_toolbar_new();
	
	//vbox = gtk_vbox_new(FALSE, 0);
	//gtk_box_pack_end(vbox, preview, TRUE, TRUE, 0);
	
	create_back(toolbar, gcommander);
	create_fwd(toolbar, gcommander);
	create_up(toolbar, gcommander);
	create_reload(toolbar, gcommander);
	create_home(toolbar, gcommander);

	//gtk_toolbar_append_widget(toolbar, vbox, "", "");
	//gtk_widget_show(preview);
	//gtk_widget_show(vbox);
	
	gnome_app_set_toolbar(GNOME_APP(gcommander->main_window), GTK_TOOLBAR(toolbar));
	
}
