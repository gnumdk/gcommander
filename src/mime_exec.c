/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "mime_exec.h"
#include "mime_ui.h"
#include "popup_menus.h"
#include "path_manager.h"
#include "group.h"
#include "list.h"

/******************************************************************************************************************************/
/*exec default command*/
void gc_exec(struct GCommander *gcommander, gchar *file)
{
	struct GC_mime *gc_mime = NULL;	
	struct GC_group *gc_group = NULL;	
		
	const gchar *ext;
	
	ext = g_extension_pointer(file);

	gc_mime = find_list_ext(gcommander->mimes, ext);
	
	if(gc_mime) 
	{
		if(gc_mime->nb_progs > 0)
			g_spawn_command_line_async(g_strconcat(gc_mime->progs[0].cmd, " " , "\"", FROM_UTF8(file), "\"", NULL), NULL);
		else
		{
			gc_group = find_list_group(gcommander->groups, gc_mime->group);
			g_spawn_command_line_async(g_strconcat(gc_group->progs[0].cmd, " " , "\"", FROM_UTF8(file), "\"", NULL), NULL);
		}
	}
	else if(is_ascii(FROM_UTF8(file)))
		g_spawn_command_line_async(g_strconcat(gcommander->default_editor, " " , "\"", FROM_UTF8(file), "\"", NULL), NULL);
}

/******************************************************************************************************************************/
/*open with selected item*/
void gc_open_with(GtkWidget *menu_item, gpointer data)
{
	struct GC_open *open = (struct GC_open *) data;
	struct GC_file *selected_file;
	struct GC_mime *gc_mime = NULL;
	struct GC_group *gc_group = NULL;	
	const gchar *ext;

	if(selected_file = selected_one(open->gcommander->view))
	{
		ext = g_extension_pointer(selected_file->name);
		gc_mime = find_list_ext(open->gcommander->mimes, ext);
		if(gc_mime)
		{
			if(gc_mime->nb_progs > 1)
				g_spawn_command_line_async(g_strconcat(gc_mime->progs[open->i].cmd, " " ,"\"",FROM_UTF8(selected_file->name), "\"", NULL), NULL);
			else
			{
				gc_group = find_list_group(open->gcommander->groups, gc_mime->group);
				g_spawn_command_line_async(g_strconcat(gc_group->progs[open->i].cmd, " " , "\"", FROM_UTF8(selected_file->name), "\"", NULL), NULL);
			}
		}
	}
}
