/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "popup_menus.h"
 
#include "callbacks.h"
#include "file_manager.h"
#include "path_manager.h"
#include "list.h"
#include "mime_ui.h"
#include "mime_exec.h"
#include "group.h"

/******************************************************************************************************************************/
/*show list pop up menu*/
void view_popup(struct GCommander *gcommander)
{
	GtkWidget *menu = NULL;
	GtkWidget *prog_menu = NULL;
	GtkWidget *open = NULL;
	GtkWidget *open_with = NULL;
	GtkWidget *copy = NULL;
	GtkWidget *cut = NULL;
	GtkWidget *paste = NULL;
	GtkWidget *delete = NULL;
	GtkWidget *type = NULL;
	GtkWidget *sep[3] = { NULL };
	GtkWidget *prog[MAX_PROGS] = { NULL };
	struct GC_file *selected_file = NULL;
	struct GC_mime *gc_mime = NULL;
	struct GC_group *gc_group = NULL;	
	struct GC_open *open_W;
	gboolean file = FALSE;
	gboolean back = FALSE;
	gboolean selected = FALSE;
	static struct GC_open *free_open[MAX_PROGS] = { NULL };
	gboolean open_with_show = FALSE;
	gboolean popup = TRUE;
	gint i = 0;
	gboolean one_sel_elem;
	
	/*we free precedent allocated submenu open with item*/
	free_open_with(free_open, prog);
	
	menu = gtk_menu_new();
	selected_file = selected_one(gcommander->view);
	one_sel_elem = one_element_selected(gcommander->view);
	if(selected_file)
	{
		if(selected_file->type == GC_FILE)
			file = TRUE;
		if(selected_file->name[0] == '.' && selected_file->name[1] == '.')
			back = TRUE;
		
		/*get prog for extension*/
		gc_mime = find_list_ext(gcommander->mimes, g_extension_pointer(selected_file->name));
		
		/*create open with submenu*/
		if(gc_mime && file)
		{	/*if we have more than one program for this mime, we have a submenu*/	
			if(gc_mime->nb_progs > 1)
			{
				prog_menu = gtk_menu_new();
				open_with = gtk_menu_item_new_with_label(_("Open with..."));	
				open_with_show = TRUE;
				/*we append item to submenu*/
				for(i=1; i<gc_mime->nb_progs; i++)
				{
					prog[i-1] = gtk_menu_item_new_with_label(gc_mime->progs[i].name);
					if((open_W = g_malloc(sizeof(struct GC_open))) == NULL)
						return;
					open_W->gcommander = gcommander;
					open_W->i = i;
					free_open[i-1] = open_W;
					g_signal_connect(G_OBJECT(prog[i-1]), "activate",
									 G_CALLBACK(gc_open_with),
									 open_W);
					gtk_widget_show(prog[i-1]);
					gtk_menu_shell_append(GTK_MENU_SHELL(prog_menu), prog[i-1]);
				}
			}/* if we have no progs, we searh in group*/
			/*FIXME: maybee find a solution to do this in precedent if*/
			else if(gc_mime->nb_progs == 0)
			{	
				gc_group = find_list_group(gcommander->groups, gc_mime->group);
				if(gc_group)
					if(gc_group->nb_progs >1 )
					{
						prog_menu = gtk_menu_new();
						open_with = gtk_menu_item_new_with_label(_("Open with..."));	
						open_with_show = TRUE;
					
						for(i=1; i<gc_group->nb_progs; i++)
						{
							prog[i-1] = gtk_menu_item_new_with_label(gc_group->progs[i].name);
							if((open_W = g_malloc(sizeof(struct GC_open))) == NULL)
								return;
							open_W->gcommander = gcommander;
							open_W->i = i;
							free_open[i-1] = open_W;
							g_signal_connect(G_OBJECT(prog[i-1]), "activate",
											 G_CALLBACK(gc_open_with),
											 open_W);
							gtk_widget_show(prog[i-1]);
							gtk_menu_shell_append(GTK_MENU_SHELL(prog_menu), prog[i-1]);
						}
					}
			}
		}

		/* Create the menu items */
		
		/*OPEN*/
		open = gtk_image_menu_item_new_from_stock(GTK_STOCK_OPEN, NULL);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), open);
		g_signal_connect(G_OBJECT(open), "activate",
						 G_CALLBACK(file_do),
						 gcommander);
		gtk_widget_show(open);

		/*if item isn't ..*/
		if(! back)
		{
			/*we append items to menu*/
			/*SEP*/
			sep[0] = gtk_separator_menu_item_new();
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), sep[0]);
			gtk_widget_show(sep[0]);
			/*OPEN WITH*/
			if(open_with)
			{
				gtk_menu_item_set_submenu(GTK_MENU_ITEM(open_with), prog_menu);
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), open_with);
				gtk_widget_show(open_with);
				/*SEP*/
				sep[1] = gtk_separator_menu_item_new();
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), sep[1]);
				gtk_widget_show(sep[1]);
			}
			
			/*COPY*/
			copy = gtk_image_menu_item_new_from_stock(GTK_STOCK_COPY, NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), copy);
			g_signal_connect(G_OBJECT(copy), "activate",
							 G_CALLBACK(clipboard_copy),
							 gcommander);
			gtk_widget_show(copy);
			/*CUT*/
			cut = gtk_image_menu_item_new_from_stock(GTK_STOCK_CUT, NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), cut);
			g_signal_connect(G_OBJECT(cut), "activate",
							 G_CALLBACK(clipboard_cut),
							 gcommander);
			gtk_widget_show(cut);
			/*PASTE*/
			if(!file && gcommander->edit.clipboard && one_sel_elem)
			{
				paste = gtk_image_menu_item_new_from_stock(GTK_STOCK_PASTE, NULL);
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), paste);
				g_signal_connect(G_OBJECT(paste), "button_press_event",
									 G_CALLBACK(action_dg_paste_callback),
									 gcommander);
				gtk_widget_show(paste);
				
			}
			/*DELETE*/
			delete = gtk_image_menu_item_new_from_stock(GTK_STOCK_DELETE, NULL);
			gtk_menu_shell_append(GTK_MENU_SHELL(menu), delete);
			g_signal_connect(G_OBJECT(delete), "button_press_event",
							 G_CALLBACK(action_dg_delete_callback),
							 gcommander);
			gtk_widget_show(delete);
			/*TYPE*/
			if(file && one_sel_elem && strchr(selected_file->name, (int) '.'))
			{
				/*SEP*/
				sep[2] = gtk_separator_menu_item_new();
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), sep[2]);
				gtk_widget_show(sep[2]);
				type = gtk_menu_item_new_with_label(_("File type"));
				gtk_menu_shell_append(GTK_MENU_SHELL(menu), type);
				g_signal_connect(G_OBJECT(type), "activate",
								 G_CALLBACK(edit_file_type),
								 gcommander);
				gtk_widget_show(type);
			}
		}
	}
	else if(gcommander->edit.clipboard)/*no file selected, popup a paste menuitem*/
	{
		paste = gtk_image_menu_item_new_from_stock(GTK_STOCK_PASTE, NULL);
		g_signal_connect(G_OBJECT(paste), "button_press_event",
						 G_CALLBACK(action_dg_paste_callback),
						 gcommander);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), paste);
		gtk_widget_show(paste);
	}
	else
		popup = FALSE;
	if(popup)
		gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 1, gtk_get_current_event_time ()); 
}
 
/*free open_with menu*/
void free_open_with(struct GC_open **free_open, GtkWidget **prog)
{
	gint i=0;
	
	while(free_open[i])
	{
		g_free(free_open[i]);
		g_free(prog[i]);
		prog[i] = NULL;
		free_open[i] = NULL;
		i++;
	}
}
