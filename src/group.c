/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "group.h"
#include "mime_ui.h"
#include "path_manager.h"

/******************************************************************************************************************************/
/*quit mime window*/
void hide_group_window(GtkWidget *widget, gpointer data)
{
	struct GCmime_group *group_item = data;
	group_item->gcommander->mime_is_show = FALSE;
	g_free(group_item);	
	gtk_widget_hide(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW));
}

/******************************************************************************************************************************/
/*create main group window*/
void group_window_new(GtkWidget *widget, struct GCommander *gcommander)
{
	GtkWidget *group_window;
	GtkWidget *scrolled;
	GtkWidget *part_box;
	GtkWidget *buttons_box;
	GtkWidget *label;
	GtkWidget *list;
	GtkWidget *CLOSE_button;
	GtkWidget *ADD_button;
	GtkWidget *REM_button;
	GtkWidget *EDIT_button;
	struct GCmime_group *group_item;
	gchar *icon;
	
	/*can't have two edit window*/
	if(gcommander->mime_is_show)
		return;
	
	if((group_item = g_malloc(sizeof(struct GCmime_group))) == NULL)
		return;
	
	gcommander->mime_is_show = TRUE;
	group_item->gcommander = gcommander;

	/*widgets creation*/
	group_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_icon_from_file(GTK_WINDOW(group_window), ICON_APP, NULL);
	gtk_window_set_transient_for(GTK_WINDOW(group_window), GTK_WINDOW(gcommander->main_window));
	gtk_window_set_position(GTK_WINDOW(group_window), GTK_WIN_POS_CENTER_ON_PARENT);
	scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                                  	GTK_POLICY_AUTOMATIC);

	gtk_window_set_resizable(GTK_WINDOW(group_window), FALSE);
	gtk_widget_set_size_request(GTK_WIDGET(group_window), GRP_W, GRP_H);
	part_box = gtk_vbox_new(FALSE, 0);
	buttons_box = gtk_hbox_new(FALSE, 0);
	label = gtk_label_new(_("Add or remove groups.\n"));
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);

	/*set group list*/
	list = gtk_tree_view_new_with_model(get_groups_model(group_item));
	group_item->list = list;
	add_group_columns(group_item);
	gtk_container_add(GTK_CONTAINER(scrolled), list);

	CLOSE_button = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
	ADD_button = gtk_button_new_from_stock(GTK_STOCK_ADD);
	REM_button = gtk_button_new_from_stock(GTK_STOCK_REMOVE);
	EDIT_button = gtk_button_new_with_label(_("Edit group"));

	/*widget association*/
	gtk_box_pack_start(GTK_BOX(buttons_box), CLOSE_button, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(buttons_box), EDIT_button, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), ADD_button, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), REM_button, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(part_box), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(part_box), scrolled, TRUE, TRUE, 10);  
	gtk_box_pack_end(GTK_BOX(part_box), buttons_box, FALSE, FALSE, 0); 

	/*callbacks*/
	g_signal_connect(G_OBJECT(group_window), "delete_event",
					 G_CALLBACK(null), NULL);
	g_signal_connect(G_OBJECT(ADD_button), "clicked",
					 G_CALLBACK(add_grp), group_item);
	g_signal_connect(G_OBJECT(REM_button), "clicked",
					 G_CALLBACK(rem_grp), group_item);	
	g_signal_connect(G_OBJECT(CLOSE_button), "clicked",
					 G_CALLBACK(hide_group_window), group_item);
	g_signal_connect(G_OBJECT(EDIT_button), "clicked",
					 G_CALLBACK(edit_file_group), group_item);				 
	gtk_container_add(GTK_CONTAINER(group_window), part_box);
	
	gtk_widget_show_all(group_window);
}

/******************************************************************************************************************************/
/*group list store*/
GtkTreeModel *get_groups_model(struct GCmime_group *mime_group)
{
	GtkListStore *store;
	
	/* create list store */
	store = gtk_list_store_new(GRP_COLUMNS, G_TYPE_STRING, G_TYPE_BOOLEAN);
	
	if(mime_group->gcommander->groups)
		g_slist_foreach(mime_group->gcommander->groups, group_append_foreach, store);

	return GTK_TREE_MODEL(store);
}

/******************************************************************************************************************************/
/*add data to store*/
void group_append_foreach(gpointer data, gpointer store)
{
	GtkTreeIter iter;
	struct GC_group *group_item = (struct GC_group *) data;
	gchar *type;	

	if(strcmp(group_item->name, DEFAULT_GRP))
	{
		gtk_list_store_append (store, &iter);
		gtk_list_store_set(store, &iter,
						   GRP_NAME, group_item->name,
						   GRP_EDITABLE, TRUE,
						   -1);
	}
}

/******************************************************************************************************************************/
/*add columns to group list*/
void add_group_columns(gpointer data)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	struct GCmime_group *group_item = (struct GCmime_group *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(group_item->list));

	renderer = gtk_cell_renderer_text_new();
	g_signal_connect (G_OBJECT(renderer), "edited", G_CALLBACK(cell_grp_edited), group_item);
	column = gtk_tree_view_column_new_with_attributes(_("Name"),
													   renderer,
													   "text", GRP_NAME,
													   "editable", GRP_EDITABLE,
													   NULL);

	g_object_set_data(G_OBJECT(renderer), "column", (gint *)GRP_NAME);
	gtk_tree_view_append_column(GTK_TREE_VIEW(group_item->list), column);
}

/******************************************************************************************************************************/
/*add a new row to list*/
void add_grp(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_group *group_item = (struct GCmime_group *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(group_item->list));
	struct GC_group *gc_group;

	/*add default group name*/
	if(! find_list_group(group_item->gcommander->groups, _("Enter group name here"))) /*#define ??*/
	{
		gtk_list_store_append(GTK_LIST_STORE(model), &iter);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter,
						   GRP_NAME, _("Enter group name here"),
						   GRP_EDITABLE, TRUE,
						   -1);
		if((gc_group = g_malloc(sizeof(struct GC_group))) == NULL)
			return;
		gc_group->name = g_strdup(_("Enter group name here"));
		gc_group->icon = NULL;
		gc_group->nb_progs = 0;
		group_item->gcommander->groups = g_slist_append(group_item->gcommander->groups, gc_group);
	}
}

/******************************************************************************************************************************/
/*remove selected row from list*/
void rem_grp(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_group *gcmime_group = (struct GCmime_group *) data;
	struct GC_group *group;
	GtkTreeView *list = (GtkTreeView *) gcmime_group->list;
	GtkTreeModel *model = gtk_tree_view_get_model(list);
	GtkTreeSelection *selection = gtk_tree_view_get_selection(list);
	gchar *grp_name;
	gint i;
	gboolean found = FALSE;
	/*FIXME => USE FIND LIST*/
	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
	{
		/*we remove from list*/
		gtk_tree_model_get(model, &iter,
						   GRP_NAME, &grp_name,
						   -1);
		/*we remove from group list*/
		for(i=0; i<g_slist_length(gcmime_group->gcommander->groups) && !found; i++)
		{
			group = g_slist_nth_data(gcmime_group->gcommander->groups, i);
			if(!strcmp(group->name, grp_name))
			{
				gcmime_group->gcommander->groups = g_slist_remove(gcmime_group->gcommander->groups, group);
				g_free(group);
				found = TRUE;
			}
		}
		make_mime_default(grp_name, gcmime_group->gcommander);
		gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
		g_free(grp_name);
	}
}

/******************************************************************************************************************************/
/*callback for cellule edition*/
void cell_grp_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data)
{
	struct GCmime_group *group_item = (struct GCmime_group *)data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(group_item->list));
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	struct GC_group *gc_group;
	GtkTreeIter iter;
	gchar *old_text;
	
	/*we search group in list*/
	if(! find_list_group(group_item->gcommander->groups, new_text))
	{
		gtk_tree_model_get_iter(model, &iter, path);
	
		/*change text*/
		gtk_tree_model_get(model, &iter, GRP_NAME,
						   &old_text, -1);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter, GRP_NAME,
						   g_strdup(new_text), -1);
		if(gc_group = find_list_group(group_item->gcommander->groups, old_text))
		{
			gchar *to_free_text = gc_group->name;
			gc_group->name = g_strdup(new_text);
			g_free(to_free_text);
		}
		/*else*/
			/*aie!*/
		g_free(old_text);
	}
	gtk_tree_path_free(path);
}

/******************************************************************************************************************************/
/*read prog list for this group*/
void group_list_set(struct GCmime_item *mime_item, gchar *group)
{
	GtkTreeIter iter;
	gboolean found = FALSE;
	struct GC_group *gc_group;
	gint i = 0;

	/* create list store */
	mime_item->model = GTK_TREE_MODEL(gtk_list_store_new(	MIME_COLUMNS,
								G_TYPE_STRING,
								G_TYPE_STRING,
								G_TYPE_STRING,
								G_TYPE_BOOLEAN   ));
	
	gc_group = find_list_group(mime_item->gcommander->groups, group);
	/*add progs to list*/
	if(gc_group)
		for(i=0; i<gc_group->nb_progs; i++)
			mime_append_prog(&gc_group->progs[i], mime_item->model);
}

/******************************************************************************************************************************/
/*save group progs in gcommander struct*/
void save_prog_group(GtkWidget *widget, gpointer data)
{
	struct GCmime_item *mime_item = data;
	struct GC_group *gc_group; 
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mime_item->list));
	GtkTreePath *path;
	GtkTreeIter iter;
	gchar *name;
	gchar *cmd;

	path = gtk_tree_path_new_first();
	
	if((gc_group = find_list_group(mime_item->gcommander->groups, mime_item->name)) != NULL)
	{
		gc_group->nb_progs = 0;
		while(gtk_tree_model_get_iter(model, &iter, path))
		{
			gtk_tree_model_get(model, &iter,
							   GC_NAME, &name,
							   GC_CMD, &cmd,	
							   -1);
			strcpy(gc_group->progs[gc_group->nb_progs].name, name);
			strcpy(gc_group->progs[gc_group->nb_progs++].cmd, cmd);
			gtk_tree_path_next(path);
			g_free(name);
			g_free(cmd);
		}
		/*we free icon and set new*/
		if(gc_group->icon)
			g_free(gc_group->icon);
	
		gc_group->icon = gnome_icon_entry_get_filename(GNOME_ICON_ENTRY(mime_item->icon));
	}
	mime_item->gcommander->clear_icons = TRUE;
	mime_item->gcommander->mime_is_show = FALSE;
	gtk_widget_hide(gtk_widget_get_ancestor(mime_item->list, GTK_TYPE_WINDOW));
}

/******************************************************************************************************************************/
/*find group in list, NULL if not found*/
struct GC_group *find_list_group(GSList *list, const gchar *group)
{
	struct GC_group *gc_group; 
	gint i;
	gboolean found = FALSE;
	
	if(list)
		for(i=0; i<g_slist_length(list) && !found; i++)
		{
			gc_group = g_slist_nth_data(list, i);
			if(!strncasecmp(gc_group->name, group, LG_EXT))
				found = TRUE;
		}
		
	if(found)
		return gc_group;
	else 
		return NULL;
	
}

/******************************************************************************************************************************/
/*return group icon NULL if not found*/
gchar *find_icon_grp(struct GCommander *gcommander, gchar *group)
{
	struct GC_group *gc_group;	
	gchar *icon = NULL;	
	
	if(gc_group = find_list_group(gcommander->groups, group))
		return gc_group->icon;
	else
		return NULL;
}

/******************************************************************************************************************************/
/*edit selected file group*/
void edit_file_group(GtkWidget *widget, gpointer data)
{
	struct GCmime_group *group_item = (struct GCmime_group *) data;
	gchar *group;
	GtkTreeIter iter;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(group_item->list));
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(group_item->list));

	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
	{
		gtk_tree_model_get(model, &iter,
						   GRP_NAME, &group,
						   -1);
		group_item->gcommander->mime_is_show = FALSE;
		mime_window_prog_new(group_item->gcommander, group, WGROUP);
	}
}

/******************************************************************************************************************************/
/*set group menu*/
void set_group_menu(GtkWidget *optmenu, struct GCommander *gcommander)
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	struct GC_group *group;
	GSList *group_radio = NULL;
	gint i;
	
	menu = gtk_menu_new();
	for(i=0; i<g_slist_length(gcommander->groups); i++)
	{
		group = g_slist_nth_data(gcommander->groups, i);
		menuitem = gtk_radio_menu_item_new_with_label(group_radio, group->name);
		group_radio = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(menuitem));
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
	}
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), menu);
}

/******************************************************************************************************************************/
/*get group index*/
gint group_index(gchar *group_name, struct GCommander *gcommander)
{
	gint i;
	struct GC_group *group;
	gboolean found = FALSE;
	
	for(i=0; i<g_slist_length(gcommander->groups) && !found; i++)
	{
		group = g_slist_nth_data(gcommander->groups, i);
		if(!strcmp(group->name, group_name))
			found = TRUE;
	}

	if(! found)
		return 0;
	else
		return i-1;
}

/******************************************************************************************************************************/
/*get group name*/
gchar *group_name(gint index, struct GCommander *gcommander)
{/*#FIXME*/
	//gint i;
	struct GC_group *group;
	
	group = g_slist_nth_data(gcommander->groups, index);
	return group->name;
}

/******************************************************************************************************************************/
/*make all mimes which belong to group belong to default group #FIXME faire une phrase qui veut dire quelque chose :)*/
void make_mime_default(gchar *group, struct GCommander *gcommander)
{
	struct GC_mime *mime;
	gint i;
	
	for(i=0; i<g_slist_length(gcommander->mimes); i++)
	{
		mime = g_slist_nth_data(gcommander->mimes, i);
		if(!strcmp(mime->group, group))
		{
			gchar *tmp = mime->group;
			mime->group = g_strdup(DEFAULT_GRP);
			g_free(tmp);
		}
	}
}


