/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "callbacks.h"

/******************************************************************************************************************************/
/*Quit program*/
void gcommander_quit(GtkWidget *widget, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
		
	if(save_config(gcommander) == -1)
		fprintf(stdin, "Config can't be save!\n");
    gcommander->quit = TRUE;
}

/******************************************************************************************************************************/
/*show hiden files*/
void show_hiden_files(GtkWidget *widget, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
	if(gcommander->show_hiden)
		gcommander->show_hiden = FALSE;
	else
		gcommander->show_hiden = TRUE;
	update_list_view(gcommander);
}

/******************************************************************************************************************************/
/*Change click policy*/
void ch_click_policy(GtkWidget *widget, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
	if(gcommander->double_click)
		gcommander->double_click = FALSE;
	else
		gcommander->double_click = TRUE;
	
}

/******************************************************************************************************************************/
/*show about*/
void about()
{
	const gchar *authors[] = {
								N_("Bellegarde Cedric"),
								NULL
							 };
	const char *documenters [] = {
									NULL
	  							 }; 
	const char *translator_credits = _("translator_credits");
	GtkWidget *about;
	GdkPixbuf *icon_app =NULL;
	GdkPixbuf *big_icon_app = NULL;
	
	/*don't understand why i have to scale image, someone have an idea?*/
	icon_app =  gdk_pixbuf_new_from_file(ICON_APP, NULL);
	if(icon_app)
	{
		big_icon_app = gdk_pixbuf_scale_simple(icon_app, 100, 100, GDK_INTERP_BILINEAR);
		g_object_unref(icon_app);
	}
	
	authors[0]=_(authors[0]);
	about = gnome_about_new(_("Gcommander"), VERSION,
				_("Copyleft 2003 GnuMdk"),
				_("A simple file manager for GNOME and GNU/Linux"),
				authors,
				documenters,
				strcmp (translator_credits, "translator_credits") != 0 ? translator_credits : NULL,
                        	big_icon_app);

	if(big_icon_app)
		gtk_window_set_icon(GTK_WINDOW(about), big_icon_app);
		
	gtk_widget_show(about);
}

/******************************************************************************************************************************/
/*reload directory*/
void reload(GtkWidget *widget, struct GCommander *gcommander)
{
	update_list_view(gcommander);
}

