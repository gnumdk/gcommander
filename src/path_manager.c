/*  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <dirent.h>
#include <string.h>
#include "path_manager.h"
#include "utils.h"
#include "mime_ui.h"

/******************************************************************************************************************************/
/*used by g_slist_insert_sorted, you know what it do*/
gint file_cmp(gconstpointer a, gconstpointer b)
{
	gint compar;
	gboolean back = FALSE;
	struct GC_file *gc_a, *gc_b;
		
	gc_a = (struct GC_file *) a;
	gc_b = (struct GC_file *) b;
	
	compar = strcmp(gc_a->name, gc_b->name);
	/*see if a is ".."*/
	if(gc_a->name[0] == '.')
		if(gc_a->name[1] == '.')
			back = TRUE;	
		
	if((gc_a->type == GC_DIR || gc_a->type == GC_LINK_DIR)&& (gc_b->type == GC_FILE || gc_b->type == GC_LINK_FILE || gc_b->type == GC_LINK_BROKEN))
		return -1;
	else if((gc_a->type == GC_FILE || gc_a->type == GC_LINK_FILE || gc_a->type == GC_LINK_BROKEN) && (gc_b->type == GC_DIR || gc_b->type == GC_LINK_DIR))
		return 1;
	else if(compar>0 && !back )
		return 1;
	else if(compar<0 || back)
		return -1;
	else return 0;
}

/******************************************************************************************************************************/
/*Return files and directories list for path.
Sort is DIR_FIRST | ALPHA_SORT |INV_ALPHA_SORT*/
GSList *list_path(gchar *path, gint sort, struct GCommander *gcommander)
{
	GSList *list = NULL; /*files path list*/
	DIR *list_dir; /*directory to list*/
	struct dirent *dir_ent;
	struct GC_file *gc_file;
	static gchar *test;	
	static GdkPixbuf *icons_static[ICON_NB] = { NULL };
	static GdkPixbuf *linked = NULL;
	static GSList *linked_list = NULL;
	GdkPixbuf *icon;
	enum file_type type;

	if(linked_list)
	{
		g_slist_free(linked_list);
		linked_list = NULL;
	}
	
	if(gcommander->clear_icons)
	{
		g_slist_free(gcommander->icon_list);
		gcommander->icon_list = NULL;
		gcommander->clear_icons = FALSE;
	}
	
	/*we load this icons at start up*/
	if(! icons_static[ICON_FILE]) 
	{
		icons_static[ICON_FILE] = gdk_pixbuf_new_scaled_from_file(DEFAULT_FILE_ICON, gcommander);
		icons_static[ICON_DIR] = gdk_pixbuf_new_scaled_from_file(DEFAULT_DIR_ICON, gcommander);
		icons_static[ICON_LINK] = gdk_pixbuf_new_from_file(DEFAULT_SYMLINK_ICON, NULL);
	}
	
	if((list_dir = opendir(path)) == NULL)
		return NULL;

	gcommander->gc_statusbar.nb_dirs = 0;
	gcommander->gc_statusbar.nb_files = 0;
	
	/*reading directory*/
	while((dir_ent = readdir(list_dir)) != NULL)
	{
		if(	/*we don't want '.' and show hiden files if it is to do*/
			!(dir_ent->d_name[0] == '.' && (dir_ent->d_name[1] == '\0' || (dir_ent->d_name[1] == '.' && dir_ent->d_name[2] == '\0'))) &&
			(!(dir_ent->d_name[0] == '.'  && dir_ent->d_name[1] !=  '.') || gcommander->show_hiden)
		  )
		{
			/*who are you to go in /dev , /boot, and /proc with this kind of software? Be damned or run a terminal
			 If you really want to be damned, there is a solution :)*/
			if( ! ((path[0] == '/' && path[1] == '\0') &&
		       ( !strcmp(dir_ent->d_name, "dev") || !strcmp(dir_ent->d_name, "proc") || !strcmp(dir_ent->d_name, "boot"))))
			{
				if(g_file_test(dir_ent->d_name, G_FILE_TEST_IS_DIR))
				{
					type = GC_DIR;
					gcommander->gc_statusbar.nb_dirs++;
					icon = icons_static[ICON_DIR];
				}
				else if(g_file_test(dir_ent->d_name, G_FILE_TEST_IS_REGULAR))
				{	
					if(! (icon = icon_get_from_ext(gcommander, dir_ent->d_name)))
					{
						/*it seems to be a shared lib, so...*/
						if(strstr(gchar_lower(dir_ent->d_name), ".so."))
						{
							icon = icon_get_from_ext(gcommander, "Bakounine.so");
						}
						else
						{
							icon = no_reconize_get_icon(dir_ent->d_name, icons_static, gcommander);
						
							/*no icon found*/
							if(!icon)
							{	
								if(is_ascii(dir_ent->d_name))
								{
									icon = icons_static[ICON_FILE];
								}
								else
								{
									if(icons_static[ICON_EXEC])
										icon = icons_static[ICON_EXEC];
									else
										icon = (icons_static[ICON_EXEC] = gdk_pixbuf_new_scaled_from_file(DEFAULT_EXEC_ICON, gcommander));
										
								}
							}
						}
					}
					type = GC_FILE;
					gcommander->gc_statusbar.nb_files++;
				}
				else
				{
					icon = icons_static[ICON_FILE];
				}
						
				if((gc_file = g_malloc(sizeof(struct GC_file))) == NULL)
						return NULL; /*allocation error*/
				
				gc_file->name = TO_UTF8(dir_ent->d_name);
				if(g_file_test(dir_ent->d_name, G_FILE_TEST_IS_SYMLINK))
				{
					linked = gdk_pixbuf_copy(icon);
					linked_list = g_slist_append(linked_list, linked);
					gdk_pixbuf_copy_area(icons_static[ICON_LINK], 0, 0, gdk_pixbuf_get_width(icons_static[ICON_LINK]), gdk_pixbuf_get_height(icons_static[ICON_LINK]), linked, 0 , 0);
					gc_file->icon = linked;
					if(type == GC_FILE)
						gc_file->type = GC_LINK_FILE;
					else if(type == GC_DIR)
						gc_file->type = GC_LINK_DIR;
					else
						gc_file->type = type;
				}
				else
				{
					gc_file->type = type;
					gc_file->icon = icon;
				}

				/*list append*/
				list = g_slist_append(list, gc_file);
			}
		}
	}
	closedir(list_dir);
	return list;
}

/******************************************************************************************************************************/
/*find icon pixbuf*/
GdkPixbuf *find_pixbuf(GSList *icons, gchar *ext)
{
	struct GC_icon *gc_icon;
	gint i;
	gboolean found = FALSE;
	
	/*if list empty*/
	if(! icons || ext[0] == '\0') return NULL;
	for(i=0; i<g_slist_length(icons) && !found; i++)
	{
		gc_icon = g_slist_nth_data(icons, i);
		if(!strcmp(ext, gc_icon->ext))
		{
			found = TRUE;
		}
	}

	if(found)
	{
		return gc_icon->icon;
	}
	else
	{
		return NULL;
	}
}

/******************************************************************************************************************************/
/*TRUE if ext is an image extension*/
gboolean *is_image(gchar *ext)
{
//return !strcasecmp(ext, "jpg") || !strcasecmp(ext, "png") || !strcasecmp(ext, "gif") || !strcasecmp(ext, "bmp") || !strcasecmp(ext, "xpm");
}

/******************************************************************************************************************************/
/*return icon  pixbuf if found, else NULL*/
GdkPixbuf *icon_get_from_ext(struct GCommander *gcommander, gchar *file)
{
	GdkPixbuf *icon;
	struct GC_icon *gc_icon;	
	gchar *ext = (gchar *) g_extension_pointer(file);
	
	/*if(is_image(ext))
	{
		if((icon = gdk_pixbuf_new_from_file(file, NULL)) != NULL)
		{
			GdkPixbuf *tmp_icon;

			gdouble fact = 32.0/gdk_pixbuf_get_height(icon);
			
			tmp_icon = gdk_pixbuf_scale_simple(icon, gdk_pixbuf_get_width(icon)*fact, 32, GDK_INTERP_BILINEAR);
			gdk_pixbuf_unref(icon);
			if(gdk_pixbuf_get_width(tmp_icon)>48)
			{
				fact = 32.0/gdk_pixbuf_get_width(tmp_icon);
				icon = gdk_pixbuf_scale_simple(tmp_icon, 48, gdk_pixbuf_get_height(tmp_icon)*fact, GDK_INTERP_BILINEAR);
				gdk_pixbuf_unref(tmp_icon);
			}
			else
				icon = tmp_icon;
		}
		else
			icon = NULL;
	}
	else */
	if(! (icon = find_pixbuf(gcommander->icon_list, ext)))
	{
		
		gchar *icon_path;
		if(icon_path = find_icon(gcommander, ext))
		{
			if((icon = gdk_pixbuf_new_scaled_from_file(icon_path, gcommander)) != NULL)
			{
				if((gc_icon = g_malloc(sizeof(struct GC_icon))) == NULL)
					return;
				gc_icon->icon = icon;
				gc_icon->ext = g_strdup(ext);
				gcommander->icon_list = g_slist_prepend(gcommander->icon_list, gc_icon);
			}
			else
				icon = NULL;
		}
		else 
			icon = NULL;
	}
	
	return icon;
}

/******************************************************************************************************************************/
/*Try to get icon of a file not being reconizes with extension*/
GdkPixbuf *no_reconize_get_icon(gchar *filename, GdkPixbuf **icons, struct GCommander *gcommander)
{
	GdkPixbuf *icon = NULL;
	gchar *tmp;
	
	if(!strcasecmp(filename, "AUTHORS"))
	{
		if(icons[ICON_AUTHORS])
			icon = icons[ICON_AUTHORS];
		else
			icon = (icons[ICON_AUTHORS] = gdk_pixbuf_new_scaled_from_file(DEFAULT_AUTHORS_ICON, gcommander));
	}
	else if(strstr(gchar_lower(filename), "makefile"))
	{
		if(icons[ICON_MAKEFILE])
			icon = icons[ICON_MAKEFILE];
		else
			icon = (icons[ICON_MAKEFILE] = gdk_pixbuf_new_scaled_from_file(DEFAULT_MAKEFILE_ICON, gcommander));
	}
	else if(strstr(gchar_lower(filename), "core"))
	{
		if(icons[ICON_CORE])
			icon = icons[ICON_CORE];
		else
			icon = (icons[ICON_CORE] = gdk_pixbuf_new_scaled_from_file(DEFAULT_CORE_ICON, gcommander));
	}
	else if(!strcasecmp(filename, "NEWS"))
	{
		if(icons[ICON_NEWS])
			icon = icons[ICON_NEWS];
		else
			icon = (icons[ICON_NEWS] = gdk_pixbuf_new_scaled_from_file(DEFAULT_NEWS_ICON, gcommander));
	}
	else if(!strcasecmp(filename, "Changelog"))
	{
		if(icons[ICON_CHANGELOG])
			icon = icons[ICON_CHANGELOG];
		else
			icon = (icons[ICON_CHANGELOG] = gdk_pixbuf_new_scaled_from_file(DEFAULT_CHANGELOG_ICON, gcommander));
	}
	else if(!strcasecmp(filename, "COPYING"))
	{	
		if(icons[ICON_COPYING])
			icon = icons[ICON_COPYING];
		else
			icon = (icons[ICON_COPYING] = gdk_pixbuf_new_scaled_from_file(DEFAULT_COPYING_ICON, gcommander));
	}
	else if(!strcasecmp(filename, "INSTALL"))
	{
		if(icons[ICON_INSTALL])
			icon = icons[ICON_INSTALL];
		else
			icon = (icons[ICON_INSTALL] = gdk_pixbuf_new_scaled_from_file(DEFAULT_INSTALL_ICON, gcommander));
	}
	else if(strstr(gchar_lower(filename), "readme"))
	{
		if(icons[ICON_README])
			icon = icons[ICON_README];
		else
			icon = (icons[ICON_README] = gdk_pixbuf_new_scaled_from_file(DEFAULT_README_ICON, gcommander));
	}
	else if(!strcasecmp(filename, "TODO"))
	{
		if(icons[ICON_TODO])
			icon = icons[ICON_TODO];
		else
			icon = (icons[ICON_TODO] = gdk_pixbuf_new_scaled_from_file(DEFAULT_TODO_ICON, gcommander));
	}
	else
		icon = NULL;

	return icon;
}

/******************************************************************************************************************************/
/*load a pixbuf and scale it if we are in list view*/
GdkPixbuf *gdk_pixbuf_new_scaled_from_file(gchar *filepath, struct GCommander *gcommander)
{
	GdkPixbuf *pixbuf;
	GdkPixbuf *scaled = NULL;
	
	if((pixbuf = gdk_pixbuf_new_from_file(filepath, NULL)) != NULL)
	{
		gdouble fact = 40.0/gdk_pixbuf_get_height(pixbuf);
		scaled = gdk_pixbuf_scale_simple(pixbuf, gdk_pixbuf_get_width(pixbuf)*fact, 40, GDK_INTERP_BILINEAR);
		g_free(pixbuf);
	}
	return scaled;
}	
