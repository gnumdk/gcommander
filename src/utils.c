/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <dirent.h>
#include <fcntl.h>
#include "sehdr.h"
#include "utils.h"

/******************************************************************************************************************************/
/*cursor busy*/
void set_cursor_busy(GtkWidget *window)
{
	GdkCursor *cursor;

	cursor=gdk_cursor_new(GDK_WATCH);
	gdk_window_set_cursor(GTK_WIDGET(window)->window, cursor);
	gdk_cursor_destroy(cursor);
	gdk_flush();
}

/******************************************************************************************************************************/
/*cursor normal*/
void set_cursor_normal(GtkWidget *window)
{
	gdk_window_set_cursor(GTK_WIDGET(window)->window, NULL);
	gdk_flush();
}

/******************************************************************************************************************************/
/*used by g_slist_insert_sorted, you know what it do*/
gint mime_cmp(gconstpointer a, gconstpointer b)
{
	gint compar = 0;

	struct GC_mime *gc_a, *gc_b;
	gc_a = (struct GC_mime *) a;
	gc_b = (struct GC_mime *) b;
	compar = strcmp(gc_a->group, gc_b->group);

	return compar;
}

/******************************************************************************************************************************/
/*used by g_slist_insert_sorted, you know what it do*/
gint group_cmp(gconstpointer a, gconstpointer b)
{
	gint compar = 0;

	struct GC_group *gc_a, *gc_b;
	gc_a = (struct GC_group *) a;
	gc_b = (struct GC_group *) b;
	compar = strcmp(gc_a->name, gc_b->name);

	return compar;
}

/******************************************************************************************************************************/
/*return true if file is an ascii file, must work most time*/
gboolean is_ascii(gchar *filename)
{/*#FIXME*/
	gint file_desc;
	guchar buff[100];
	gboolean ascii = TRUE;
	gint nb_read;
	gint i;

	if((file_desc = open(filename, O_RDONLY)) == -1)
		return FALSE;
	
	if((nb_read = read(file_desc, &buff, sizeof(gchar)*100))>=0)
		for(i=0; i < nb_read; i++)
			if(((gint) buff[i] < 32 || (gint) buff[i] >175) && (gint) buff[i] != 10 && (gint) buff[i] != 9 && (gint) buff[i] != 0)
				ascii = FALSE;

	close(file_desc);
	return ascii;	
}

/******************************************************************************************************************************/
/*lower a string
WARNING: Will not work with local specifique strings!*/
gchar *gchar_lower(gchar *to_lower)
{
	gchar *lower;
	gint len = strlen(to_lower);
	gint i;
	
	if((lower = g_malloc(sizeof(gchar) * (len+1))) == NULL)
	{
		fprintf(stderr, "Allocation error\n");
		perror("malloc()");
		return NULL;
	}
	
	for(i=0; i<len; i++)
		lower[i] = tolower(to_lower[i]);
	return lower;
}
