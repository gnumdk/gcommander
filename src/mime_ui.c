/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "mime_ui.h"
#include "group.h"
#include "utils.h"
#include "path_manager.h"
#include "list.h"

/******************************************************************************************************************************/
/*do nothing*/
gint null(){return TRUE;}

/******************************************************************************************************************************/
/*quit mime window*/
void hide_mime_window(GtkWidget *widget, gpointer data)
{
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	mime_item->gcommander->mime_is_show = FALSE;
	g_free(mime_item);
	gtk_widget_hide(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW));
}

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*create main mime window*/
void mime_window_new(GtkWidget *widget, struct GCommander *gcommander)
{
	GtkWidget *mime_window;
	GtkWidget *scrolled;
	GtkWidget *part_box;
	GtkWidget *buttons_box;
	GtkWidget *label;
	GtkWidget *group_menu;
	GtkWidget *CLOSE_button;
	GtkWidget *ADD_button;
	GtkWidget *REM_button;
	struct GCmime_item *mime_item;
	gchar *icon;
	
	/*can't have two edit window*/
	if(gcommander->mime_is_show)
		return;
	
	if((mime_item = g_malloc(sizeof(struct GCmime_item))) == NULL)
		return;
	
	gcommander->mime_is_show = TRUE;
	mime_item->gcommander = gcommander;
	gcommander->mimes = g_slist_sort(gcommander->mimes, mime_cmp);

	/*widgets creation*/
	mime_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_icon_from_file(GTK_WINDOW(mime_window), ICON_APP, NULL);
	gtk_window_set_transient_for(GTK_WINDOW(mime_window), GTK_WINDOW(gcommander->main_window));
	gtk_window_set_position(GTK_WINDOW(mime_window), GTK_WIN_POS_CENTER_ON_PARENT);
	scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                                  	GTK_POLICY_AUTOMATIC);

	gtk_window_set_resizable(GTK_WINDOW(mime_window), FALSE);
	gtk_widget_set_size_request(GTK_WIDGET(mime_window), GRP_W, GRP_H);
	part_box = gtk_vbox_new(FALSE, 0);
	buttons_box = gtk_hbox_new(FALSE, 0);
	label = gtk_label_new(_("Add or remove mimes.\n"
						    "Select a group for each mime"));
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);

	/*create mime list*/
	mime_item->model = get_mime_model(mime_item);
	mime_item->list = gtk_tree_view_new_with_model(mime_item->model);
	mime_item->update = TRUE;
	add_mime_columns(mime_item);
	gtk_container_add(GTK_CONTAINER(scrolled), mime_item->list);

	/*create group menu*/
	group_menu = gtk_option_menu_new();
	set_group_menu(group_menu, gcommander);
	gtk_option_menu_set_history(GTK_OPTION_MENU(group_menu), group_index(DEFAULT_GRP, gcommander));
	mime_item->opt = group_menu;

	CLOSE_button = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
	ADD_button = gtk_button_new_from_stock(GTK_STOCK_ADD);
	REM_button = gtk_button_new_from_stock(GTK_STOCK_REMOVE);

	/*widget association*/
	gtk_box_pack_start(GTK_BOX(buttons_box), CLOSE_button, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(buttons_box), group_menu, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), ADD_button, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), REM_button, FALSE, FALSE, 0);

	gtk_box_pack_start(GTK_BOX(part_box), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(part_box), scrolled, TRUE, TRUE, 10);  
	gtk_box_pack_end(GTK_BOX(part_box), buttons_box, FALSE, FALSE, 0); 

	/*callbacks*/
	g_signal_connect(G_OBJECT(mime_window), "delete_event",
					 G_CALLBACK(null), NULL);
	g_signal_connect(G_OBJECT(mime_item->list), "button_release_event",
					 G_CALLBACK(mime_clicked), mime_item);
	g_signal_connect(G_OBJECT(group_menu), "changed",
					 G_CALLBACK(ch_grp_menu), mime_item);
	g_signal_connect(G_OBJECT(ADD_button), "clicked",
					 G_CALLBACK(add_mime), mime_item);
	g_signal_connect(G_OBJECT(REM_button), "clicked",
					 G_CALLBACK(rem_mime), mime_item);	
	g_signal_connect(G_OBJECT(CLOSE_button), "clicked",
					 G_CALLBACK(hide_mime_window), mime_item);	
	gtk_container_add(GTK_CONTAINER(mime_window), part_box);
	
	gtk_widget_show_all(mime_window);
	/*first element selected by default, we don't want this*/
	gtk_tree_selection_unselect_all(gtk_tree_view_get_selection(GTK_TREE_VIEW(mime_item->list)));
}

/*********************************************************************************************************************/
/*mime list store*/
GtkTreeModel *get_mime_model(struct GCmime_item *mime_item)
{
	GtkListStore *store;
	
	/* create list store */
	store = gtk_list_store_new(GRP_COLUMNS, G_TYPE_STRING, G_TYPE_BOOLEAN);
	
	/*append list store*/
	if(mime_item->gcommander->mimes)
		g_slist_foreach(mime_item->gcommander->mimes, mime_append_foreach, store);

	return GTK_TREE_MODEL(store);
}
	
/*********************************************************************************************************************/
/*add data to store*/
void mime_append_foreach(gpointer data, gpointer store)
{
	GtkTreeIter iter;
	struct GC_mime *mime = (struct GC_mime *) data;
	static gchar *old_grp = NULL;	
	

	if(old_grp == NULL)
	{
		gtk_list_store_append (store, &iter);
		gtk_list_store_set(store, &iter,
						   GRP_NAME, "*************************",
						   GRP_EDITABLE, FALSE,
						   -1);
		old_grp = mime->group;
	}
	else if(strcmp(old_grp, mime->group))
	{
		gtk_list_store_append (store, &iter);
		gtk_list_store_set(store, &iter,
						   GRP_NAME, "*************************",
						   GRP_EDITABLE, FALSE,
						   -1);
		old_grp = mime->group;
	}

	gtk_list_store_append (store, &iter);
	gtk_list_store_set(store, &iter,
					   GRP_NAME, mime->name,
					   GRP_EDITABLE, TRUE,
						-1);
}

/******************************************************************************************************************************/
/*add columns to group list*/
void add_mime_columns(gpointer data)
{
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mime_item->list));

	renderer = gtk_cell_renderer_text_new();
	g_signal_connect (G_OBJECT(renderer), "edited", G_CALLBACK(cell_mime_edited), mime_item);
	column = gtk_tree_view_column_new_with_attributes(_("Name"),
													   renderer,
													   "text", GRP_NAME,
													   "editable", GRP_EDITABLE,
													   NULL);

	g_object_set_data(G_OBJECT(renderer), "column", (gint *)GRP_NAME);
	gtk_tree_view_append_column(GTK_TREE_VIEW(mime_item->list), column);
}

/*********************************************************************************************************************/
/*add a new row to list*/
void add_mime(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mime_item->list));
	struct GC_mime *gc_mime;
		
	if(! find_list_mime(mime_item->gcommander->mimes, _("Enter mime name here")))
	{
		/*add mime to list*/
		gtk_list_store_append(GTK_LIST_STORE(model), &iter);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter,
						   GRP_NAME, _("Enter mime name here"),
						   GRP_EDITABLE, TRUE,
						   -1);
		/*add mime to mime list*/
		if((gc_mime = g_malloc(sizeof(struct GC_mime))) == NULL)
			return;
		gc_mime->name = g_strdup(_("Enter mime name here"));
		gc_mime->exts = NULL;
		gc_mime->icon = NULL;
		gc_mime->group = g_strdup(DEFAULT_GRP);
		gc_mime->nb_progs = 0;
		mime_item->gcommander->mimes = g_slist_insert_sorted(mime_item->gcommander->mimes, gc_mime, mime_cmp);
	}
}

/*********************************************************************************************************************/
/*remove selected row from list*/
void rem_mime(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	struct GC_mime *gc_mime;
	GtkTreeView *list = (GtkTreeView *) mime_item->list;
	GtkTreeModel *model = gtk_tree_view_get_model(list);
	GtkTreeSelection *selection = gtk_tree_view_get_selection(list);
	gchar *mime_name;
	gint i;
	
	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
	{
		/*we remove from mime list*/
		gtk_tree_model_get(model, &iter,
						   EXT_NAME, &mime_name,
						   -1);

		/*we remove if we find mime in mime list, if not, i will be daned*/
		if((gc_mime = find_list_mime(mime_item->gcommander->mimes, mime_name)) != NULL)
		{
			mime_item->gcommander->mimes = g_slist_remove(mime_item->gcommander->mimes, gc_mime);
			g_free(gc_mime);
			/*we remove from list*/
			gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
		}
	}
}

/******************************************************************************************************************************/
/*callback for cellule edition*/
void cell_mime_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data)
{
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mime_item->list));
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	GtkTreeIter iter;
	gchar *old_text;
	struct GC_mime *gc_mime;
	
	/*we search mime in list*/
	if(! find_list_mime(mime_item->gcommander->mimes, new_text))
	{
		gtk_tree_model_get_iter(model, &iter, path);
	
		/*change text in cell*/
		gtk_tree_model_get(model, &iter, GRP_NAME,
						   &old_text, -1);
		gtk_list_store_set(GTK_LIST_STORE(model), &iter, GRP_NAME,
						   g_strdup(new_text), -1);
		/*change mime item name*/
		if(gc_mime = find_list_mime(mime_item->gcommander->mimes, old_text))
		{
			gchar *to_free_text = gc_mime->name;
			gc_mime->name = g_strdup(new_text);
			g_free(to_free_text);
		}
		/*else*/
			/*aie!*/
	}
	gtk_tree_path_free(path);
}

/******************************************************************************************************************************/
/*group menu change*/
void ch_grp_menu(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	GtkTreeView *list = (GtkTreeView *) mime_item->list;
	GtkTreeModel *model = gtk_tree_view_get_model(list);
	GtkTreeSelection *selection = gtk_tree_view_get_selection(list);
	struct GC_mime *gc_mime;
	gchar *mime_name;

	/*we get selection*/
	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
	{
		gtk_tree_model_get(model, &iter,
						   GRP_NAME, &mime_name,
						   -1);
		/*we search selection in mime list*/
		if(gc_mime = find_list_mime(mime_item->gcommander->mimes, mime_name))
		{
			/*we change mime group*/
			if(mime_item->gcommander->groups)
				gc_mime->group = group_name(gtk_option_menu_get_history(GTK_OPTION_MENU(mime_item->opt)), mime_item->gcommander);
			else
				gc_mime->group = g_strdup(DEFAULT_GRP);
				
			if(mime_item->update == FALSE)
				mime_item->update = TRUE;
			else
			{
				mime_item->gcommander->mimes = g_slist_sort(mime_item->gcommander->mimes, mime_cmp);
				gtk_list_store_clear(GTK_LIST_STORE(mime_item->model));
				mime_item->model = get_mime_model(mime_item);
				gtk_tree_view_set_model(GTK_TREE_VIEW(mime_item->list), mime_item->model);
			}
			g_free(mime_name);
		}
		/*else*/
			/*aie!*/
	}
}

/******************************************************************************************************************************/
/*mime list clicked*/
void mime_clicked(GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	GtkTreeIter iter;
	struct GCmime_item *mime_item = (struct GCmime_item *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(widget));
	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(widget));
	struct GC_mime *gc_mime;
	gchar *coin = g_strdup(mime_item->gcommander->current_path);
	gchar *mime_name;
	
	/*we get selection*/
	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
	{
		gtk_tree_model_get(model, &iter,
						   GRP_NAME, &mime_name,
						   -1);
		
		/*we search selection in mime list*/
		if(gc_mime = find_list_mime(mime_item->gcommander->mimes, mime_name))
		{
			gint index = group_index(gc_mime->group, mime_item->gcommander);
			if(gtk_option_menu_get_history(GTK_OPTION_MENU(mime_item->opt)) !=  index)
			{
				mime_item->update = FALSE;
				/*we update group menu*/
				gtk_option_menu_set_history(GTK_OPTION_MENU(mime_item->opt), index);
			}
		}
		g_free(mime_name);
	}
}

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

/*create main mime window for extension or group name*/
void mime_window_prog_new(struct GCommander *gcommander, gchar *name, enum window_type type)
{
	GtkWidget *mime_window;
	GtkWidget *scrolled;
	GtkWidget *ext_scrolled;
	GtkWidget *part_box;
	GtkWidget *icon_box;
	GtkWidget *buttons_box;
	GtkWidget *label;
	GtkWidget *mime_menu;
	GtkWidget *list;
	GtkWidget *OK_button;
	GtkWidget *CANCEL_button;
	GtkWidget *ADD_button;
	GtkWidget *REM_button;
	GtkWidget *icon_button;
	
	struct GCmime_item *mime_item;
	struct GC_mime *gc_mime = NULL;	
	gchar *icon;
	gchar *mime_name;
	
	/*can't have two edit window*/
	if(gcommander->mime_is_show)
		return;
	
	if((mime_item = g_malloc(sizeof(struct GCmime_item))) == NULL)
		return;
	
	gcommander->mime_is_show = TRUE;
	mime_item->model = NULL;
	
	/*widgets creation*/
	mime_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_icon_from_file(GTK_WINDOW(mime_window), ICON_APP, NULL);
	gtk_window_set_transient_for(GTK_WINDOW(mime_window), GTK_WINDOW(gcommander->main_window));
	gtk_window_set_position(GTK_WINDOW(mime_window), GTK_WIN_POS_CENTER_ON_PARENT);
	scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                                  	GTK_POLICY_AUTOMATIC);
	
	gtk_window_set_resizable(GTK_WINDOW(mime_window), FALSE);
	gtk_widget_set_size_request(GTK_WIDGET(mime_window), MIME_W, MIME_H);
	part_box = gtk_vbox_new(FALSE, 0);
	/*for mime, we need another box for icon and menu*/
	if(type == WMIME)
		icon_box = gtk_hbox_new(FALSE, 0);
	
	buttons_box = gtk_hbox_new(FALSE, 0);
	
	mime_item->gcommander = gcommander;
	mime_item->menu_items = NULL;
	mime_item->icon = NULL;
	mime_item->selected_mime = NULL;
	mime_item->ext =  name;
	mime_item->name = mime_item->ext;
	
	
	if(type == WMIME)
	{
		/*we get mime item, get it group and set mime list*/
		gc_mime = find_list_ext(gcommander->mimes, name);
		if(gc_mime)
			mime_name = gc_mime->name;
		else
			mime_name = NULL;
		
		mime_list_set(mime_item, mime_name);
		label = gtk_label_new(_("Please select a type for this file.\n"
								"Please enter programs name and command for this type,"
								" first listed program is default one.\n"
								"Use drag and order column to order programs.\n"
								));
	}
	else
	{
		label = gtk_label_new(_("Please enter programs name and command for this group,"
								" first listed program is default one.\n"
								"Use drag and order column to order programs.\n"
								));
		group_list_set(mime_item, name);
	}

	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	
	/*list creation*/
	list = gtk_tree_view_new_with_model(mime_item->model);
	gtk_tree_view_set_reorderable(GTK_TREE_VIEW(list), TRUE);
	add_mime_prog_columns(list);
	gtk_container_add(GTK_CONTAINER(scrolled), list);
	OK_button = gtk_button_new_from_stock(GTK_STOCK_OK);
	CANCEL_button = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
	ADD_button = gtk_button_new_from_stock(GTK_STOCK_ADD);
	REM_button = gtk_button_new_from_stock(GTK_STOCK_REMOVE);
	
	icon_button = gnome_icon_entry_new("Gcommander", "Gcommander icons");
	gnome_icon_entry_set_pixmap_subdir(GNOME_ICON_ENTRY(icon_button), ICONS_DIR);
	
	/*icon and menu*/
	if(type == WMIME)
	{
		if(icon = find_icon_mime_from_ext(gcommander, name))
			gnome_icon_entry_set_filename(GNOME_ICON_ENTRY(icon_button), icon);
		mime_menu = gtk_option_menu_new();
		set_mime_menu(mime_menu, mime_item);
	}
	/*only icon*/
	else if(icon = find_icon_grp(gcommander, name))
	{
		gnome_icon_entry_set_filename(GNOME_ICON_ENTRY(icon_button), icon);
	}
	
	/*widget association*/
	gtk_box_pack_start(GTK_BOX(buttons_box), CANCEL_button, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(buttons_box), OK_button, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), ADD_button, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(buttons_box), REM_button, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(part_box), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(part_box), icon_button, FALSE, FALSE, 0);
	
	/*add icon and menu*/
	if(type == WMIME)
	{
		gtk_box_pack_start(GTK_BOX(part_box), icon_box, FALSE, FALSE, 0);
		gtk_box_pack_start(GTK_BOX(icon_box), mime_menu, FALSE, FALSE, 0);
		mime_item->opt = mime_menu;
	}
		
	gtk_box_pack_start(GTK_BOX(part_box), scrolled, TRUE, TRUE, 10); 
	gtk_box_pack_end(GTK_BOX(part_box), buttons_box, FALSE, FALSE, 0); 
	
	/*for OK button*/
	mime_item->list = list;
	mime_item->icon = icon_button;
	
	/*callbacks*/
	g_signal_connect(G_OBJECT(mime_window), "delete_event",
					 G_CALLBACK(null), NULL);
	g_signal_connect(G_OBJECT(ADD_button), "clicked",
					 G_CALLBACK(add_prog), list);
	g_signal_connect(G_OBJECT(REM_button), "clicked",
					 G_CALLBACK(rem_prog), list);	
	if(type == WMIME)
		g_signal_connect(G_OBJECT(OK_button), "clicked",
						 G_CALLBACK(save_prog_mime_ok), mime_item);	
	else
		g_signal_connect(G_OBJECT(OK_button), "clicked",
						 G_CALLBACK(save_prog_group), mime_item);	
	g_signal_connect(G_OBJECT(CANCEL_button), "clicked",
					 G_CALLBACK(hide_mime_window), mime_item);	
	gtk_container_add(GTK_CONTAINER(mime_window), part_box);
	
	gtk_widget_show_all(mime_window);
}

/******************************************************************************************************************************/
/*use to add value to mime list*/
void mime_append_prog(gpointer data, gpointer store)
{
	GtkTreeIter iter;
	struct GC_program *prog = data;
	gchar *type;	
	
	gtk_list_store_append (store, &iter);
	gtk_list_store_set(store, &iter,
			   GC_NAME, prog->name,
			   GC_CMD, prog->cmd,
			   GC_DRAG, NULL,
			   COLUMN_EDITABLE, TRUE,
			   -1);
}

/******************************************************************************************************************************/
/*read mime list for this extension*/
void mime_list_set(struct GCmime_item *mime_item, gchar *name)
{
	GtkTreeIter iter;
	gboolean found = FALSE;
	struct GC_mime *gc_mime;
	gint i = 0;

	/* create list store */
	if(!mime_item->model)
		mime_item->model = GTK_TREE_MODEL(gtk_list_store_new(	MIME_COLUMNS,
									G_TYPE_STRING,
									G_TYPE_STRING,
									G_TYPE_STRING,
									G_TYPE_BOOLEAN  ));
	/*get mime item*/
	if(name)
		gc_mime = find_list_mime(mime_item->gcommander->mimes, name);
	else
		gc_mime = g_slist_nth_data(mime_item->gcommander->mimes, 0);

	/*set mime progs*/
	if(gc_mime)
	{
		for(i=0; i<gc_mime->nb_progs; i++)
			mime_append_prog(&gc_mime->progs[i], mime_item->model);
	}
}

/******************************************************************************************************************************/
/*add columns to mime list*/
void add_mime_prog_columns(GtkWidget *list)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(list));

	renderer = gtk_cell_renderer_text_new ();
	g_signal_connect (G_OBJECT(renderer), "edited", G_CALLBACK(cell_edited), model);
	column = gtk_tree_view_column_new_with_attributes(_("Name"),
													   renderer,
													   "text", GC_NAME,
													   "editable", COLUMN_EDITABLE,
													   NULL);

	gtk_tree_view_column_set_min_width(GTK_TREE_VIEW_COLUMN(column), 150);
	g_object_set_data(G_OBJECT(renderer), "column", (gint *)GC_NAME);
	gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);

	renderer = gtk_cell_renderer_text_new ();
	g_signal_connect (G_OBJECT(renderer), "edited", G_CALLBACK(cell_edited), model);
	column = gtk_tree_view_column_new_with_attributes(_("Command"),
													  renderer,
													  "text", GC_CMD,
													  "editable", COLUMN_EDITABLE,													  
													  NULL);
	gtk_tree_view_column_set_min_width(GTK_TREE_VIEW_COLUMN(column), 250);
	g_object_set_data(G_OBJECT(renderer), "column", (gint *)GC_CMD);
	gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes(_("Drag and order"),
													  renderer,
													  "text", GC_DRAG,
													  NULL);
													  
	gtk_tree_view_append_column(GTK_TREE_VIEW(list), column);
	/*#FIXME gtk_tree_selection_set_mode(gtk_tree_view_get_selection(list), GTK_SELECTION_MULTIPLE);*/
}

/******************************************************************************************************************************/
/*set mime menu*/
void set_mime_menu(GtkWidget *optmenu, struct GCmime_item *mime_item)
{
	GtkWidget *menu;
	GtkWidget *submenu = NULL;
	GtkWidget *menuitem;
	GtkWidget *submenu_item;
	struct GC_group *group;
	struct GC_mime *mime;
	struct GCmime_menu *mime_menu = NULL;	
	GSList *group_radio = NULL;
	gboolean had_submenu = FALSE;
	gchar *current = NULL;
	gchar *current_grp;
	gint i, h;
	
	/*create menu and find mime item*/
	menu = gtk_menu_new();
	mime = find_list_ext(mime_item->gcommander->mimes, mime_item->name);
	/*if we mime already exist for this extension, put it in current*/
	if(mime)
		current = mime->name;

	/*foreach on list to set group item*/
	for(i=0; i<g_slist_length(mime_item->gcommander->groups); i++)
	{
		/*create a menu item with group name*/
		group = g_slist_nth_data(mime_item->gcommander->groups, i);
		menuitem = gtk_menu_item_new_with_label(group->name);
		/*set submenu*/
		if(!submenu)
			submenu =  gtk_menu_new();

		/*foreach on list to set mime subitem*/
		for(h=0; h<g_slist_length(mime_item->gcommander->mimes); h++)
		{
			mime = g_slist_nth_data(mime_item->gcommander->mimes, h);
			if(!strcmp(mime->group, group->name))
			{
				/*create a radio item*/
				submenu_item = gtk_radio_menu_item_new_with_label(group_radio, mime->name);

				g_signal_connect(G_OBJECT(submenu_item), "activate",
								 G_CALLBACK(submenu_selected), mime_item);
	
				/*add menu item to list=>for saving mime*/
				if((mime_menu = g_malloc(sizeof(struct GCmime_menu))) == NULL)
					return;
	
				mime_menu->addr = (gint) submenu_item;
				mime_menu->name = g_strdup(mime->name);
				mime_item->menu_items = g_slist_append(mime_item->menu_items, mime_menu);
				
				/*if we have the good mime, select it*/
				if(current)
				{
					if(!strcmp(mime->name, current))
					{
						mime_item->selected_mime = g_strdup(current);
						gtk_menu_item_activate(GTK_MENU_ITEM(submenu_item));
						current_grp = mime->group;
					}
				}

				group_radio = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(submenu_item));
				gtk_menu_shell_append(GTK_MENU_SHELL(submenu), submenu_item);
				had_submenu = TRUE;
			}
		}
		/*if no submenu, insensitive*/
		if(!had_submenu)
			gtk_widget_set_sensitive(menuitem, FALSE);
		else
		{
			had_submenu = FALSE;
		}
		
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), submenu);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
		
		submenu = NULL;
	}
	gtk_option_menu_set_menu(GTK_OPTION_MENU(optmenu), menu);
	
	/*set group menu*/
	if(current)
		gtk_option_menu_set_history(GTK_OPTION_MENU(optmenu), group_index(current_grp, mime_item->gcommander));
}


/******************************************************************************************************************************/
/*mime item menu selected*/
void submenu_selected(GtkWidget *widget, gpointer data)
{
	struct GCmime_item *mime_item = data;
	struct GCmime_menu *mime_menu;	
	gchar *old_name;
	gchar *icon;
	gboolean found = FALSE;
	gint i;
	
	if(mime_item->icon)
	{
		if(mime_item->menu_items != NULL)
			for(i=0; i<g_slist_length(mime_item->menu_items) && !found; i++)
			{
				mime_menu = g_slist_nth_data(mime_item->menu_items, i);
				/*if it is our search widget*/
				if(mime_menu->addr == (gint) widget)
				{
					old_name = mime_item->selected_mime;
					mime_item->selected_mime = g_strdup(mime_menu->name);
					if(old_name)
						g_free(old_name);
					found = TRUE;
				}
			}
		/*we update view*/	
		if(found)
		{
			gtk_list_store_clear(GTK_LIST_STORE(mime_item->model));
			mime_list_set(mime_item, mime_menu->name);
			/*we update icon*/
			if(icon = find_icon_mime(mime_item->gcommander, mime_menu->name))
				gnome_icon_entry_set_filename(GNOME_ICON_ENTRY(mime_item->icon), icon);
			else
				gnome_icon_entry_set_filename(GNOME_ICON_ENTRY(mime_item->icon), "");
				
		}
	}
}

/******************************************************************************************************************************/
/*add a new row to list*/
void add_prog(GtkWidget *widget, gpointer data)
{
  GtkTreeIter iter;
  GtkTreeModel *model = gtk_tree_view_get_model(data);

  gtk_list_store_append(GTK_LIST_STORE(model), &iter);
  gtk_list_store_set(GTK_LIST_STORE(model), &iter,
		      GC_NAME, _("Enter program name here"),
		      GC_CMD, _("Enter program command here"),
			  GC_DRAG, NULL,
		      COLUMN_EDITABLE, TRUE,
		      -1);
}

/******************************************************************************************************************************/
/*remove selected row from list*/
void rem_prog(GtkWidget *widget, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeView *list = (GtkTreeView *)data;
	GtkTreeModel *model = gtk_tree_view_get_model(list);
	GtkTreeSelection *selection = gtk_tree_view_get_selection(list);

	if(gtk_tree_selection_get_selected(selection, NULL, &iter))
		gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
}

/******************************************************************************************************************************/
/*callback for cellule edition*/
void cell_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data)
{
	GtkTreeModel *model = (GtkTreeModel *)data;
	GtkTreePath *path = gtk_tree_path_new_from_string(path_string);
	GtkTreeIter iter;
	gint *column;

	column = g_object_get_data(G_OBJECT(cell), "column");

	gtk_tree_model_get_iter(model, &iter, path);

	/*set name or command*/
	switch(GPOINTER_TO_INT(column))
	{
		case GC_NAME:
		{
			gint i;

			i = gtk_tree_path_get_indices(path)[0];

			gtk_list_store_set(GTK_LIST_STORE(model), &iter, column,
							   g_strdup(new_text), -1);
		}
		break;

		case GC_CMD:
		{
			gint i;
			gchar *old_text;

			i = gtk_tree_path_get_indices(path)[0];

			gtk_list_store_set(GTK_LIST_STORE(model), &iter, column,
							   g_strdup(new_text), -1);
		}	
		break;
    }

  gtk_tree_path_free (path);
}

/******************************************************************************************************************************/
/*save mime type in gcommander struct and hide window*/
void save_prog_mime_apply(GtkWidget *widget, gpointer data)
{
	struct GCmime_item *mime_item = data;
	struct GC_mime *gc_mime; 
	struct GC_mime *clear_mime;	
	struct GC_group *gc_group; 
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mime_item->list));
	GtkTreePath *path;
	GtkTreeIter iter;
	gint *submenu; /*submenu number*/
	gchar *mime;
	gchar *name;
	gchar *cmd;

	path = gtk_tree_path_new_first();

	/*update mime*/
	if(gc_mime = find_list_mime(mime_item->gcommander->mimes, mime_item->selected_mime))
	{
		gc_mime->nb_progs = 0;
		/*if this extension already belong to another mime, remove it*/
		clear_mime_ext(mime_item, mime_item->ext);
		gc_mime->exts = g_slist_append(gc_mime->exts, g_strdup(mime_item->ext));

		while(gtk_tree_model_get_iter(model, &iter, path))
		{
			gtk_tree_model_get(model, &iter,
							   GC_NAME, &name,
							   GC_CMD, &cmd,	
							   -1);
			strcpy(gc_mime->progs[gc_mime->nb_progs].name, name);
			strcpy(gc_mime->progs[gc_mime->nb_progs++].cmd, cmd);
			g_free(name);
			g_free(cmd);
			gtk_tree_path_next(path);
		}
		gc_mime->icon = gnome_icon_entry_get_filename(GNOME_ICON_ENTRY(mime_item->icon));

	}
	else 
		fprintf(stderr, "Error in save_prog_mime()");
	mime_item->gcommander->clear_icons = TRUE;
	mime_item->gcommander->mime_is_show = FALSE;
}

/******************************************************************************************************************************/
/*clear this extension ext belong to a mime, remove it*/
void clear_mime_ext(struct GCmime_item *mime_item, gchar *ext)
{
	struct GC_mime *gc_mime = find_list_ext(mime_item->gcommander->mimes, ext);
	gchar *extension;
	gboolean found = FALSE;
	gint i;
	
	if(gc_mime)
	{
		/*search ext*/
		for(i=0; i<g_slist_length(gc_mime->exts); i++)
		{
			extension = g_slist_nth_data(gc_mime->exts, i);
			if(!strcmp(extension, ext))
				found = TRUE;
		}
		gc_mime->exts = g_slist_remove(gc_mime->exts, extension);
	}
	
}

/******************************************************************************************************************************/
/*save mime type in gcommander struct*/
void save_prog_mime_ok(GtkWidget *widget, gpointer data)
{
	struct GCmime_item *mime_item = data;
	save_prog_mime_apply(widget, data);
	gtk_widget_hide(gtk_widget_get_ancestor(mime_item->list, GTK_TYPE_WINDOW));
}

/******************************************************************************************************************************/
/*find extension in list, NULL if not found*/
struct GC_mime *find_list_ext(GSList *list, const gchar *ext)
{
	struct GC_mime *gc_mime; 
	gchar *extension;
	gint i, h;
	gboolean found = FALSE;
	
	if(list)
		/*iter on mime list*/
		for(i=0; i<g_slist_length(list) && !found; i++)
		{
			gc_mime = g_slist_nth_data(list, i);

			/*iter on exts list*/ 
			if(gc_mime->exts)
			{
				for(h=0; h<g_slist_length(gc_mime->exts); h++)
				{
					extension = g_slist_nth_data(gc_mime->exts, h);
					if(!strcasecmp(ext, extension))
					{	
						found = TRUE;
					}
				}
			}
		}
	if(found)
		return gc_mime;
	else 
		return NULL;
	
}

/******************************************************************************************************************************/
/*find name in list, NULL if not found*/
struct GC_mime *find_list_mime(GSList *list, const gchar *name)
{
	struct GC_mime *gc_mime; 
	gint i;
	gboolean found = FALSE;

	if(list)
		for(i=0; i<g_slist_length(list) && !found; i++)
		{
			gc_mime = g_slist_nth_data(list, i);
			if(!strcmp(gc_mime->name, name))
				found = TRUE;
		}
	if(found)
	{
		return gc_mime;
	}
	else 
		return NULL;
	
}

/******************************************************************************************************************************/
/*return mime icon, group if not found, NULL if not belong to a group*/
gchar *find_icon(struct GCommander *gcommander, const gchar *ext)
{
	struct GC_mime *gc_mime; 
	struct GC_group *gc_group;	
	gchar *icon = NULL;	
	gint i;
	gboolean found = FALSE;
	
	gc_mime = find_list_ext(gcommander->mimes, ext);
	if(gc_mime)
	{
		if(gc_mime->icon == NULL)
		{
			gc_group = find_list_group(gcommander->groups, gc_mime->group);
			if(gc_group)
				if(gc_group->icon)
					icon = gc_group->icon;
		}
		else
			icon = gc_mime->icon;
	}
	return icon;
	
}

/******************************************************************************************************************************/
/*return icon NULL if not found*/
gchar *find_icon_mime(struct GCommander *gcommander, const gchar *name)
{
	struct GC_mime *gc_mime;	
	gchar *icon = NULL;	
	
	if(gc_mime = find_list_mime(gcommander->mimes, name))
		return gc_mime->icon;
	else
		return NULL;
}

/******************************************************************************************************************************/
/*return icon NULL if not found*/
gchar *find_icon_mime_from_ext(struct GCommander *gcommander, const gchar *ext)
{
	struct GC_mime *gc_mime;	
	gchar *icon = NULL;	
	
	if(gc_mime = find_list_ext(gcommander->mimes, ext))
		return gc_mime->icon;
	else
		return NULL;
}

/******************************************************************************************************************************/
/*edit selected file type*/
void edit_file_type(GtkWidget *widget, gpointer data)
{
	struct GC_file *file;
	struct GCommander *gcommander = (struct GCommander *) data;
		
	file = selected_one(gcommander->view);
	if(file)
		mime_window_prog_new(gcommander, (gchar *) g_extension_pointer(file->name), WMIME);
}

/******************************************************************************************************************************/
/*get mime index*/
gint mime_index(gchar *name, struct GCommander *gcommander)
{
	gint i;
	struct GC_mime *mime;
	gboolean found = FALSE;
	
	for(i=0; i<g_slist_length(gcommander->mimes) && !found; i++)
	{
		mime = g_slist_nth_data(gcommander->mimes, i);
		if(!strcmp(mime->name, name))
			found = TRUE;
	}

	if(! found)
		return 0;
	else
		return i-1;
}

/******************************************************************************************************************************/
/*get mime name*/
gchar *mime_name(gint *index, struct GCommander *gcommander)
{
	gint i;
	struct GC_mime *mime;
	
	mime = g_slist_nth_data(gcommander->mimes, i);
	return mime->name;
}
