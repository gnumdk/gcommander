/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gcommander.h"
#include "callbacks.h"
#include "path_manager.h"
#include "location.h"

/******************************************************************************************************************************/
/*some initializations*/
void init(struct GCommander *gcommander)
{
	strcpy(gcommander->current_path, "/home/gnumdk");
	gcommander->model = NULL;
	gcommander->show_hiden = FALSE;
	gcommander->backward = NULL;
	gcommander->forward = NULL;
	gcommander->double_click = FALSE;
	gcommander->edit.to_paste = NULL;
	gcommander->edit.to_delete = NULL;
	gcommander->icon_list = NULL;
	gcommander->mimes = NULL;
	gcommander->groups = NULL;
	gcommander->mime_is_show = FALSE;
	gcommander->default_editor = g_strdup("emacs");
	gcommander->quit = FALSE;
	if(read_config(gcommander) == -1)
		fprintf(stdin, "Config can't be load!\n");
}

/******************************************************************************************************************************/
/*Create main window*/
struct GCommander *main_window_new()
{
	struct GCommander *gcommander;
	GtkWidget *vbox;	
	GtkWidget *location_box;

	if((gcommander = g_malloc(sizeof(struct GCommander))) == NULL)
		return NULL;
	
	if(chdir("/home/gnumdk") == -1)
		return NULL;
	
	init(gcommander);
	
	gcommander->main_window = gnome_app_new(APP_NAME, "Gestion de fichier");
	gtk_window_set_default_size(GTK_WINDOW(gcommander->main_window), WIDTH, HEIGHT);
	g_signal_connect(G_OBJECT(gcommander->main_window), "destroy",
					 G_CALLBACK(gcommander_quit), gcommander);
	
	list_view_new(gcommander);
	menus_new(gcommander);
	toolbar_new(gcommander);
	statusbar_new(gcommander);
	set_statusbar(gcommander);
	
	location_box = GTK_WIDGET(create_location(gcommander));
	gtk_entry_set_text(GTK_ENTRY(gcommander->location), TO_UTF8(gcommander->current_path));
	
	gcommander->scrolled = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (gcommander->scrolled),
                                  	GTK_POLICY_AUTOMATIC,
                                  	GTK_POLICY_AUTOMATIC);
	
	gtk_container_add(GTK_CONTAINER(gcommander->scrolled), gcommander->view);
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), location_box, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(vbox), gcommander->scrolled, TRUE, TRUE, 0);
										
	gnome_app_set_contents(GNOME_APP(gcommander->main_window), GTK_WIDGET(vbox));	
	gnome_window_icon_set_from_file(GTK_WINDOW(gcommander->main_window), ICON_APP);
	gtk_widget_show_all(gcommander->main_window);
	
	return gcommander;
	
}

