/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "gcommander.h"
#include "location.h"
#include "mime_ui.h"

/******************************************************************************************************************************/
/*create location bar*/
GtkWidget *create_location(struct GCommander *gcommander)
{
	GtkWidget *hbox;
	GtkWidget *label;
	
	hbox = gtk_hbox_new(FALSE, 0);
	label = gtk_label_new(_("Location: "));
	gcommander->location = gtk_entry_new();
	
	g_signal_connect(G_OBJECT(gcommander->location), "activate",
			 G_CALLBACK(location_action), gcommander);
	
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(hbox), gcommander->location, TRUE, TRUE, 0);
	
	return hbox;
}

/******************************************************************************************************************************/
/*go to file*/
void location_action(GtkWidget *widget, gpointer data)
{
	struct GCommander *gcommander = (struct GCommander *) data;
	gchar *file = (gchar *) gtk_entry_get_text(GTK_ENTRY(gcommander->location));
	struct stat statbuff;
		
	if(stat(g_filename_from_utf8(file, -1, NULL, NULL, NULL), &statbuff) == -1)
		return;
	
	if(S_ISDIR(statbuff.st_mode))
	{
		if(chdir(g_filename_from_utf8(file, -1, NULL, NULL, NULL)) != -1)
		{
			gcommander->forward = NULL;
			gcommander->backward = g_slist_prepend(gcommander->backward, g_strdup(gcommander->current_path));
			strncpy(gcommander->current_path, g_filename_from_utf8(file, -1, NULL, NULL, NULL), LG_PATH);
			update_list_view(gcommander);
		}
	}
	else
	{
		struct GC_mime *gc_mime;
		gchar *ext;
	
		ext = (gchar *) g_extension_pointer(file);
	
		gc_mime = find_list_ext(gcommander->mimes, ext);
		if(gc_mime) 
			gc_exec(gcommander, file);
		else
			mime_window_prog_new(gcommander, ext, WMIME);
	}
}
