/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "action_dg.h"
#include "file_manager.h"
#include "list.h"

/******************************************************************************************************************************/
/*Create an action dialog*/
void action_dg_new(struct GCommander *gcommander, enum files_actions action, gint button)
{
	GtkWidget *scrolled;
	GtkWidget *part_box;
	GtkWidget *label_box;
	GtkWidget *buttons_box;
	GtkWidget *label;
	GtkWidget *OK_button;
	GtkWidget *Cancel_button;
	GtkWidget *icon;
	GSList *files;
	GSList *selected_files = NULL;
	struct GCaction_dg *action_dg;
	gboolean quit = FALSE;

	/*for deleting files, we need something to be selected*/
	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)),
									get_selection_foreach, 
									&selected_files);
	
	/*we don't have selected files, we leave*/
	if(! selected_files && action == DELETE_ACTION)
		return;
	else
		if(selected_files)
			g_slist_free(selected_files);

	if((action_dg = g_malloc(sizeof(struct GCaction_dg))) == NULL)
		return;

	action_dg->gcommander = gcommander;
	action_dg->window = NULL;
	
	if(action == DELETE_ACTION)
	{
		 /*we get files to delete*/
		action_dg->delete_files = get_to_delete_files(gcommander);
		if(button == 3) /*we have to create a files list*/
			action_dg->view = gtk_tree_view_new_with_model(get_to_delete_model(action_dg->delete_files));
		else /*we just tell user if he is sure*/
			action_dg->view = NULL;
	}
	else /*PASTE ACTION*/
	{
		GtkTreeModel *model;
		action_dg->paste_files = get_to_paste_files(gcommander); /*we get files to paste*/
		action_dg->paste_files->to_path = g_strdup(gcommander->current_path);
		action_dg->paste_files->cut = gcommander->edit.cut;
		model = get_to_paste_model(action_dg); 
		
		if(! model) /*no files to owerwrite*/ 
		{
			/*we paste without dialog window*/
			action_start_paste(NULL, action_dg);
			quit = TRUE;
		}
		else
		{
			/*we tell user files to erase*/
			if(button == 0)
			{	/*we just tell him that files vill be erased*/
				action_dg->view = NULL;
				g_free(model);
			}
			else
			{
				/*we show him files list*/
				action_dg->view = gtk_tree_view_new_with_model(model);
			}
		}
	}

	if(!quit) /*if quit==TRUE, no files to erased, we quit*/
	{
		/*widgets creation*/
		action_dg->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_window_set_icon_from_file(GTK_WINDOW(action_dg->window), ICON_APP, NULL);
		gtk_window_set_transient_for(GTK_WINDOW(action_dg->window), GTK_WINDOW(gcommander->main_window));
		gtk_window_set_position(GTK_WINDOW(action_dg->window), GTK_WIN_POS_CENTER_ON_PARENT);
		gtk_window_set_resizable(GTK_WINDOW(action_dg->window), FALSE);
		
		if(button == 3) /*create files list*/
		{
			scrolled = gtk_scrolled_window_new(NULL, NULL);
			gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
										   GTK_POLICY_AUTOMATIC,
										   GTK_POLICY_AUTOMATIC);
			add_file_columns(action_dg);
			gtk_container_add(GTK_CONTAINER(scrolled), action_dg->view);
			gtk_widget_set_size_request(GTK_WIDGET(action_dg->window), ACTION_W, ACTION_H);
		}
	
		OK_button = gtk_button_new_from_stock(GTK_STOCK_OK);
		Cancel_button = gtk_button_new_from_stock(GTK_STOCK_CANCEL);
	
		buttons_box = gtk_hbox_new(FALSE, 0);
		gtk_box_pack_end(GTK_BOX(buttons_box), OK_button, FALSE, FALSE, 0);
		gtk_box_pack_end(GTK_BOX(buttons_box), Cancel_button, FALSE, FALSE, 0);
	
		if(action == DELETE_ACTION)
			label = gtk_label_new(_("Do you really want to delete this files?"));
		else
			label = gtk_label_new(_("Files will be erased, are you sure?"));
		
		label_box = gtk_hbox_new(FALSE, 0);
		icon = gtk_image_new_from_stock(GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
		gtk_box_pack_start(GTK_BOX(label_box), icon, TRUE, TRUE, 0);
		gtk_box_pack_start(GTK_BOX(label_box), label, TRUE, TRUE, 0);

		part_box = gtk_vbox_new(FALSE, 0);
		gtk_box_pack_start(GTK_BOX(part_box), label_box, FALSE, FALSE, 0);
		if(button == 3)
			gtk_box_pack_start(GTK_BOX(part_box), scrolled, TRUE, TRUE, 0);
		gtk_box_pack_end(GTK_BOX(part_box), buttons_box, FALSE, FALSE, 0);
	
		if(action == DELETE_ACTION)
			g_signal_connect(G_OBJECT(OK_button), "clicked",
							 G_CALLBACK(action_start_remove), action_dg);
		else
			g_signal_connect(G_OBJECT(OK_button), "clicked",
							 G_CALLBACK(action_start_paste), action_dg);
		g_signal_connect(G_OBJECT(Cancel_button), "clicked",
						 G_CALLBACK(action_cancel), action_dg);
	
		gtk_container_add(GTK_CONTAINER(action_dg->window), part_box);
		gtk_widget_show_all(action_dg->window);
	}
}

/******************************************************************************************************************************/
/*delete action dialog callback*/
GSList *get_to_delete_files(struct GCommander *gcommander)
{
	GSList *selected_files = NULL;
	GSList *files = NULL;
	GSList *rec_files = NULL;
	struct GC_file *file;
	gchar *path = g_strdup(gcommander->current_path);
	gint i;

	/*we get selected files*/
	gtk_tree_selection_selected_foreach(gtk_tree_view_get_selection(GTK_TREE_VIEW(gcommander->view)),
									get_selection_foreach, 
									&selected_files);
	
	for(i=0; i<g_slist_length(selected_files); i++)
	{
		file = g_slist_nth_data(selected_files, i);
		/*if directory, we get dir content*/
		if(file->type == GC_DIR)
		{
			rec_files = get_rec_files(file, path);
			/*add new files to files list*/
			if(files)
			{
				/*if we have found files*/
				if(rec_files)
					files = g_slist_concat(files, rec_files);
			}
			/*create files list*/
			else
			{
				files = rec_files;
			}
		}
		/*add file to files list*/
		else
		{
			file->path = path;
			files = g_slist_append(files, file);
		}
	}
	return files;
}

/******************************************************************************************************************************/
/*delete action dialog callback*/
struct GC_paste *get_to_paste_files(struct GCommander *gcommander)
{
	GSList *files = NULL;
	GSList *rec_files = NULL;
	struct GC_file *file;
	struct GC_paste *paste_files = NULL;	
	gint i;
	
	for(i=0; i<g_slist_length(gcommander->edit.clipboard); i++)
	{
		file = g_slist_nth_data(gcommander->edit.clipboard, i);

		/*if directory, we get dir content*/
		if(g_file_test(g_strconcat(gcommander->edit.from_path, "/", file->name, NULL), G_FILE_TEST_IS_DIR))
		{
			rec_files = get_rec_files(file, gcommander->edit.from_path);
			/*add new files to files list*/
			if(files)
			{
				/*if we have found files*/
				if(rec_files)
					files = g_slist_concat(files, rec_files);
			}
			/*create files list*/
			else
			{
				files = rec_files;
			}
		}
		/*add file to files list*/
		else
		{
			file->type = GC_FILE;
			file->path = gcommander->edit.from_path;
			files = g_slist_append(files, file);
		}
	}
	
	if(files)
	{
		if((paste_files = g_malloc(sizeof(struct GC_paste))) == NULL)
			return;
		
		paste_files->files = files;
	}
	return paste_files;
	
}

/******************************************************************************************************************************/
/*file list store*/
GtkTreeModel *get_to_delete_model(GSList *files)
{
	GtkListStore *store;

	/* create list store */
	store = gtk_list_store_new(Q_COLUMNS, G_TYPE_BOOLEAN, G_TYPE_STRING);
	/*append list store*/
	if(files)
		g_slist_foreach(files, file_append_foreach, store);

	return GTK_TREE_MODEL(store);
}

/******************************************************************************************************************************/
/*file list store*/
GtkTreeModel *get_to_paste_model(struct GCaction_dg *action_dg)
{
	GtkListStore *store = NULL;
	GtkTreeIter iter;
	struct GC_file *file;
	struct GC_file *selected_file;
	gint i;
	
	selected_file = selected_one(action_dg->gcommander->view);

	if(selected_file)
		action_dg->gcommander->edit.selected = g_strdup(selected_file->name);
	else
		action_dg->gcommander->edit.selected = NULL;
		
	if(action_dg->paste_files)
	{
		for(i=0; i<g_slist_length(action_dg->paste_files->files); i++)
		{
			/*if a dir is selected, we have to paste file into, else, we paste in current path*/
			gchar *formated_file_name;
			file = g_slist_nth_data(action_dg->paste_files->files, i);
	
			if(selected_file)
				formated_file_name = g_strconcat(selected_file->name, "/", file->name, NULL);
			else
				formated_file_name = file->name;
	
			/*we put this in the view if exist*/
			if(!g_file_test(formated_file_name, G_FILE_TEST_IS_DIR) && g_file_test(formated_file_name, G_FILE_TEST_EXISTS))
			{
				if(!store)
					/* create list store */
					store = gtk_list_store_new(Q_COLUMNS, G_TYPE_BOOLEAN, G_TYPE_STRING);
				gtk_list_store_append(store, &iter);

				gtk_list_store_set(store, &iter,
						   Q_CHECK, TRUE,
						   Q_NAME, TO_UTF8(formated_file_name),
						   -1);
			}
		
		}
	}
	return GTK_TREE_MODEL(store);
}

/******************************************************************************************************************************/
/*add data to store*/
void file_append_foreach(gpointer data, gpointer store)
{
	GtkTreeIter iter;
	struct GC_file *file = (struct GC_file *) data;

	gtk_list_store_append (store, &iter);

	gtk_list_store_set(store, &iter,
					   Q_CHECK, TRUE,
					   Q_NAME, TO_UTF8(file->name),
					   -1);
}
	
/******************************************************************************************************************************/
/*add columns to file list*/
void add_file_columns(struct GCaction_dg *action_dg)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(action_dg->view));

	renderer = gtk_cell_renderer_toggle_new();
	g_signal_connect(G_OBJECT(renderer), "toggled", G_CALLBACK(ch_toggled), action_dg);
	column = gtk_tree_view_column_new_with_attributes(_("Select"),
							  renderer,
							  "active",
							  Q_CHECK,
													   NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(action_dg->view), column);
	
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(_("Name"),
													   renderer,
													   "text", Q_NAME,
													   NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(action_dg->view), column);
}

/******************************************************************************************************************************/
/*hide action window*/
void action_cancel(GtkWidget *widget, gpointer data)
{
	struct GCaction_dg *action_dg = (struct GCaction_dg *) data;
	g_free(action_dg);
	gtk_widget_hide(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW));
}

/******************************************************************************************************************************/
/*delete action dialog callback*/
void action_dg_delete_callback(GtkWidget *widget, GdkEventButton *event, struct GCommander *gcommander)
{
	action_dg_new(gcommander, DELETE_ACTION, event->button);
}

/******************************************************************************************************************************/
/*delete action dialog callback*/
void action_dg_delete_menu_callback(GtkWidget *widget, struct GCommander *gcommander)
{
	action_dg_new(gcommander, DELETE_ACTION, 3);
}

/******************************************************************************************************************************/
/*paste action dialog callback*/
void action_dg_paste_callback(GtkWidget *widget, GdkEventButton *event, struct GCommander *gcommander)
{
	action_dg_new(gcommander, PASTE_ACTION, event->button);
}

/******************************************************************************************************************************/
/*paste action dialog callback*/
void action_dg_paste_menu_callback(GtkWidget *widget, struct GCommander *gcommander)
{
	action_dg_new(gcommander, PASTE_ACTION, 3);
}

/******************************************************************************************************************************/
/*callback for toggled button*/
void ch_toggled(GtkCellRendererToggle *cell, gchar *path_str, gpointer data)
{
	struct GCaction_dg *action_dg = (struct GCaction_dg *) data;
	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(action_dg->view));
	GtkTreeIter  iter;
	GtkTreePath *path = gtk_tree_path_new_from_string(path_str);
	gchar *name;
	gboolean fixed;

	/* get toggled iter */
	gtk_tree_model_get_iter(model, &iter, path);
	gtk_tree_model_get(model, &iter, Q_CHECK, &fixed, -1);
	gtk_tree_model_get(model, &iter, Q_NAME, &name, -1);
	
	/* do something with the value */
	fixed ^= 1;
	/* set new value */
	gtk_list_store_set(GTK_LIST_STORE(model), &iter, Q_CHECK, fixed, -1);

	/* clean up */
	g_free(name);
	gtk_tree_path_free(path);
}

/******************************************************************************************************************************/
/*remove name from files list, gboolean to true if we have to look to basename*/
GSList *remove_from_list(GSList *files, gchar *name, gboolean basename)
{
	struct GC_file *file;
	gint i;
	gchar *tmp;
	gboolean found = FALSE;
	
printf("%s\n", name);
	for(i=0; i<g_slist_length(files) && !found; i++)
	{
		file = g_slist_nth_data(files, i);
		if(basename)
			tmp = (gchar *) g_basename(file->name);
		else
			tmp = file->name;
		printf("%s\n", name);
		if(!strcmp(tmp, name))
		{
			files = g_slist_remove(files, file);
			found = TRUE;
		}
		//if(basename) g_free(tmp);
	}
	if(g_slist_length(files) == 0)
	{
		g_free(files);
		files = NULL;
	}
	return files;
}
