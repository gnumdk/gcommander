#!/bin/bash
#je déconseille la lecture de ce scripts aux personnes malades en voiture :)
ICONS=/home/gnumdk/Projects/Gcommander-0.1/pixmaps
grp_file="$HOME/.gc/grp.data"
mime_file="$HOME/.gc/mime.data"
tmp_file=/tmp/gc.$$
echo "//name">$grp_file
echo "//icon">>$grp_file
echo "//number of commands">>$grp_file
echo "//command name">>$grp_file
echo "//command">>$grp_file
echo "//If you edit this file, don't add blank line!">>$grp_file

echo "//name">$mime_file
echo "//extension">>$mime_file
echo "//icon">>$mime_file
echo "//group">>$mime_file
echo "//number of commands">>$mime_file
echo "//command name">>$mime_file
echo "//command">>$mime_file
echo "//If you edit this file, don't add blank line!">>$mime_file


for file in  grp/*.dat
do
	>$tmp_file
	OLD_IFS=$IFS
 	IFS=/
	while read name cmd_find cmd
	do
		if [[ "$name" = "" || "$cmd_find" = "" || "$cmd" = "" ]] ;then
			echo "$file : this file add blank or incomplet lines!"
		else
			IFS=" "
			whereis $cmd_find | while read the_cmd where
			do
				if [[ "$where" != "" ]] ;then
					echo "$name">>$tmp_file
					echo "$cmd">>$tmp_file
					echo "$name => $cmd"
				fi
			done
			IFS="/"
		fi
	done<"$file"
  	export IFS=$OLD_IFS
	echo "/****************************************************/">>$grp_file
	echo $(expr "$file" : 'grp/\([^.]*\).*')>>$grp_file
	echo $ICONS/$(expr "$file" : 'grp/\([^.]*\).*').png>>$grp_file
  	nb_progs=$(expr "$(wc -l $tmp_file)" : '[^[0-9]*\([0-9]*\).*')
	(( nb_progs/=2 ))
	echo $nb_progs>>$grp_file
	cat $tmp_file>>$grp_file
	#add mimes
	cat mime_grp/$(expr "$file" : 'grp/\([^.]*\).*').mime | while read mime_name
	do
		#we add read prog for mime
		>$tmp_file
	        OLD_IFS=$IFS
	        IFS=/
		[[ -f mime/"$(expr "$file" : 'grp/\([^.]*\).*')"/"$mime_name.dat" ]] && \
		while read name cmd_find cmd
	        do
		        if [[ "$name" = "" || "$cmd_find" = "" || "$cmd" = "" ]] ;then
				echo "$file : this file add blank or incomplet lines!"
		        else
											                        IFS=" "
				whereis $cmd_find | while read the_cmd where
				do
					if [[ "$where" != "" ]] ;then
						echo "$name">>$tmp_file
		                                echo "$cmd">>$tmp_file
		                                
		                        fi
		                done
		                IFS="/"
			fi
		done<mime/"$(expr "$file" : 'grp/\([^.]*\).*')"/"$mime_name.dat"
		export IFS=$OLD_IFS
		echo "/******************************************/">>$mime_file
		echo "$mime_name">>$mime_file
		cat ext/$(expr "$file" : 'grp/\([^.]*\).*')/"$mime_name".dat>>$mime_file
		if [[ -f $ICONS/"$mime_name".png ]]; then
			echo $ICONS/"$mime_name".png>>$mime_file
		else
			echo "(null)">>$mime_file
		fi
		echo $(expr "$file" : 'grp/\([^.]*\).*')>>$mime_file
		nb_progs=$(expr "$(wc -l $tmp_file)" : '[^[0-9]*\([0-9]*\).*')
		(( nb_progs/=2 ))
		echo $nb_progs>>$mime_file
		cat $tmp_file>>$mime_file
	done
done
rm -f $tmp_file

