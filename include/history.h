/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include "gcommander.h"

#ifndef __HISTORY_H
#define __HISTORY_H

enum history_actions
{
	GO_BACK,
	GO_FWD,
	GO_UP,
	GO_HOME
};

/*move in backward directory*/
void history_go_back(GtkWidget *widget, struct GCommander *gcommander);

/*move in forward directory*/
void history_go_fwd(GtkWidget *widget, struct GCommander *gcommander);

/*move in .. directory*/
void history_go_up(GtkWidget *widget, struct GCommander *gcommander);

/*move in home directory*/
void go_home(GtkWidget *widget, struct GCommander *gcommander);

/*move in history with action*/
void history_go(struct GCommander *gcommander, enum history_actions action);

#endif
