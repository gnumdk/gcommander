/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GC_GROUP_H
#define __GC_GROUP_H

#include "gcommander.h"

#define GRP_W 350
#define GRP_H 250

enum 
{
	GRP_NAME,
	GRP_EDITABLE,
	GRP_COLUMNS
};

struct GCmime_group
{
	struct GCommander *gcommander;
	GSList *group;	
	gchar *name;
	GtkWidget *icon;
	GtkWidget *list;
};

/*create main group window*/
void group_window_new(GtkWidget *widget, struct GCommander *gcommander);

/*groups list store*/
GtkTreeModel *get_groups_model(struct GCmime_group *mime_group);
	
/*add data to store*/
void group_append_foreach(gpointer data, gpointer store);

/*add columns to group list*/
void add_group_columns(gpointer data);

/*add a new row to list*/
void add_grp(GtkWidget *widget, gpointer data);

/*remove selected row from list*/
void rem_grp(GtkWidget *widget, gpointer data);

/*callback for cellule edition*/
void cell_grp_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data);

/*read prog list for this group*/
//void group_list_set(struct GCmime_item *mime_item, gchar *group);
	
/*save group progs in gcommander struct*/
void save_prog_group(GtkWidget *widget, gpointer data);

/*edit selected file group*/
void edit_file_group(GtkWidget *widget, gpointer data);

/*set group menu*/
void set_group_menu(GtkWidget *optmenu, struct GCommander *gcommander);
	
/*get group index*/
gint group_index(gchar *group_name, struct GCommander *gcommander);
	
/*get group name*/
gchar* group_name(gint *index, struct GCommander *gcommander);
	
/*return group icon NULL if not found*/
gchar *find_icon_grp(struct GCommander *gcommander, gchar *group);
	
/*find group in list, NULL if not found*/
struct GC_group *find_list_group(GSList *list, gchar *group);
	
/*make all mimes which belong to group belong to default group #FIXME faire une phrase qui veut dire quelque chose :)*/
void make_mime_default(gchar *group, struct GCommander *gcommander);
	
#endif
