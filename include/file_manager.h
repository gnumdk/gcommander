/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __FILE_MANAGER_H
#define __FILE_MANAGER_H

#include "gcommander.h"
#include "path_manager.h"
#include "action_dg.h"

#define COPY_BUFF 1024


/*Add an selected item to data list*/
void get_selection_foreach(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);

/*copy selected files*/
void clipboard_copy(GtkWidget *widget, struct GCommander *gcommander);
	
/*cut selected files*/
void clipboard_cut(GtkWidget *widget, struct GCommander *gcommander);

/*Remove toggled files*/
void action_start_remove(GtkWidget *widget, gpointer data);

/*paste copied/cuted files*/
void action_start_paste(GtkWidget *widget, gpointer data);

/*return recursive file list*/
GSList *get_rec_files(struct GC_file *file, gchar *path);

/*copy dest from src*/
int copy(gchar *dest, gchar *src);

#endif
