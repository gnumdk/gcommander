/*
   ELF: a general ELF tool

   Author : Samy Al Bahra
   Website: http://www.kerneled.com
   E-mail : samy@kerneled.com

   License: GPL (read COPYING for details)
*/


/* General static ELF-related information */
/*Bellegarde Cedric: not complet file, i don't need all defines for my soft*/

#define EI_NIDENT (16)

#define ET_NONE         0               /* No file type */
#define ET_REL          1               /* Relocatable file */
#define ET_EXEC         2               /* Executable file */
#define ET_DYN          3               /* Shared object file */
#define ET_CORE         4               /* Core file */
#define ET_NUM          5               /* Number of defined types */
#define ET_LOOS         0xfe00          /* OS-specific range start */
#define ET_HIOS         0xfeff          /* OS-specific range end */
#define ET_LOPROC       0xff00          /* Processor-specific range start */
#define ET_HIPROC       0xffff          /* Processor-specific range end */


/* ehdr */
struct elf32_ehdr{
  unsigned char e_ident[EI_NIDENT];          /* Magic number and other info */
  unsigned short int e_type;                 /* Object file type */
  unsigned short int e_machine;              /* Architecture */
  unsigned long e_version;                   /* Object file version */
  unsigned long e_entry;                     /* Entry point virtual address */
  unsigned long e_phoff;                     /* Program header table file offset */
  unsigned long e_shoff;                     /* Section header table file offset */
  unsigned long e_flags;                     /* Processor-specific flags */
  unsigned short int e_ehsize;               /* ELF header size in bytes */
  unsigned short int e_phentsize;            /* Program header table entry size */
  unsigned short int e_phnum;                /* Program header table entry count */
  unsigned short int e_shentsize;            /* Section header table entry size */
  unsigned short int e_shnum;                /* Section header table entry count */
  unsigned short int e_shstrndx;             /* Section header string table index */
    char *se_class;			     /* String representation of object sizes */
    char *se_data;			     /* String representation of data encoding */
    char *se_abi;			     /* String representation of ABI */
    char *se_type;			     /* String representation of file type */
    char *se_machine;			     /* String representation of architecture */
    char *se_version;                        /* String representation of ELF version */
};


