/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __LIST_H
#define __LIST_H
#include "gcommander.h"


enum
{
	FILE_ICON,
	FILE_NAME,
	FILE_TYPE,
	NB_COLUMNS
};

/*create file list view*/
void list_view_new(struct GCommander *gcommander);

/*update file list view*/
void update_list_view(struct GCommander *gcommander);

/*remove file list view*/
void remove_list_view(struct GCommander *gcommander);

/*add columns to treeview and set callbacks*/
void add_columns_set_callbacks(struct GCommander *gcommander);

/*create a list of file*/
void create_file_list(struct GCommander *gcommander);

/*fucking nautilus function, free software powa!*/
void tree_view_get_selection_foreach_func(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);

/*get the first selected file*/
struct GC_file *selected_one(GtkWidget *list);
	
/*return true if we have only one element selected*/
gboolean *one_element_selected(GtkWidget *list);

/*use by create_file_list to add values to list*/
void list_append_foreach(gpointer data, gpointer store);

/*go to directory path*/
void list_clicked(GtkWidget *tree, GdkEventButton *event, gpointer data);

/*perform action on selected list file*/
void file_do(GtkWidget *widget, gpointer data);

/*select all files*/
void select_all(GtkWidget *widget, gpointer data);

/*disable default code
taken from nautilus, thanx :)*/
gboolean button_press_callback(GtkWidget *widget, GdkEventButton *event, gpointer data);
#endif
