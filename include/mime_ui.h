/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MIME_UI_H
#define __MIME_UI_H

#include "gcommander.h"

#define MIME_W 550
#define MIME_H 300

enum
{
	GC_NAME,
	GC_CMD,
	GC_DRAG,
	COLUMN_EDITABLE,
	MIME_COLUMNS
};

enum
{
	EXT_NAME,
	EXT_EDITABLE,
	EXT_COLUMNS
};

enum window_type
{
	WGROUP,
	WMIME
};

struct GCmime_menu
{
	gint addr;
	gchar *name;
};

/*used to pass two params to Ok button callback*/
struct GCmime_item
{
	struct GCommander *gcommander;
	GSList *menu_items;	
	GtkWidget *list;
	GtkTreeModel *model;
	GtkWidget *opt;
	GtkWidget *icon;
	gchar *selected_mime;
	gboolean update; //if true, update view
	gchar *ext;//FIXME
	gchar *name;//FIXME
};

/*do nothing*/
gint null();

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*create main mime window*/
void mime_window_new(GtkWidget *widget, struct GCommander *gcommander);

/*mime list store*/
GtkTreeModel *get_mime_model(struct GCmime_item *mime_item);
	
/*add data to store*/
void mime_append_foreach(gpointer data, gpointer store);

/*add columns to group list*/
void add_mime_columns(gpointer data);

/*add a new row to list*/
void add_mime(GtkWidget *widget, gpointer data);

/*remove selected row from list*/
void rem_mime(GtkWidget *widget, gpointer data);

/*callback for cellule edition*/
void cell_mime_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data);

/*menu change*/
void ch_grp_menu(GtkWidget *widget, gpointer data);

/*mime list clicked*/
void mime_clicked(GtkWidget *widget, GdkEventButton *event, gpointer data);

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*create main mime window for extension or group name*/
void mime_window_prog_new(struct GCommander *gcommander, gchar *name, enum window_type type);

/*read mime list for this extension*/
void mime_list_set(struct GCmime_item *mime_item, gchar *name);
	
/*add columns to mime list*/
void add_mime_prog_columns(GtkWidget *list);

/*set mime menu*/
void set_mime_menu(GtkWidget *optmenu, struct GCmime_item *mime_item);
	
/*mime item menu selected*/
void submenu_selected(GtkWidget *widget, gpointer data);

/*add a new row to list*/
void add_prog(GtkWidget *widget, gpointer data);

/*remove selected row from list*/
void rem_prog(GtkWidget *widget, gpointer data);

/*callback for cellule edition*/
void cell_edited(GtkCellRendererText *cell, const gchar *path_string, const gchar *new_text, gpointer data);

/*save mime type in gcommander struct and hide window*/
void save_prog_mime_apply(GtkWidget *widget, gpointer data);

/*clear this extension ext belong to a mime, remove it*/
void clear_mime_ext(struct GCmime_item *mime_item, gchar *ext);
	
/*save mime type in gcommander struct*/
void save_prog_mime_ok(GtkWidget *widget, gpointer data);

/*find extension in list, NULL if not found*/
struct GC_mime *find_list_ext(GSList *list, gchar *ext);
	
/*find name in list, NULL if not found*/
struct GC_mime *find_list_mime(GSList *list, gchar *name);
	
/*return mime icon, group if not found, NULL if not belong to a group*/
gchar *find_icon(struct GCommander *gcommander, gchar *ext);

/*return icon NULL if not found*/
gchar *find_icon_mime(struct GCommander *gcommander, gchar *name);

/*return mime icon NULL if not found*/
gchar *find_icon_mime_from_ext(struct GCommander *gcommander, gchar *ext);
	
/*edit selected file type*/
void edit_file_type(GtkWidget *widget, gpointer data);

/*get mime index*/
gint mime_index(gchar *name, struct GCommander *gcommander);
	
/*get mime name*/
gchar* mime_name(gint *index, struct GCommander *gcommander);

#endif
