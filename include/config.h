/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __GC_CONFIG_H
#define __GC_CONFIG_H
#include "gcommander.h"

#define CFG ".gc"
#define MIME_CFG ".gc/mime.data"
#define GRP_CFG ".gc/grp.data"

/*read config*/
gint read_config(struct GCommander *gcommander);
	
/*save config*/
gint save_config(struct GCommander *gcommander);
	
/*read mime config*/
gint read_mime_dd(struct GCommander *gcommander);

/*save mime config*/
gint save_mime_dd(struct GCommander *gcommander);
	
/*read group config*/
gint read_group_dd(struct GCommander *gcommander);
	
/*read group config*/
gint save_group_dd(struct GCommander *gcommander);

/*convert a string in a list of word*/
GSList *char2exts(gchar *buff);

/*convert a list of word in string*/
gchar *exts2char(GSList *exts);

#endif
