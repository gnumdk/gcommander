/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __PATH_MANAGER_H
#define __PATH_MANAGER_H
#include "gcommander.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

enum 
{
	DIR_FIRST, /*directories come first*/
	ALPHA_SORT, /*alphabetical sort*/
	INV_ALPHA_SORT, /*inverted(??) alphabetical sort*/
};

enum file_type
{
	GC_DIR,
	GC_FILE,
	GC_LINK_DIR,
	GC_LINK_FILE,
	GC_LINK_BROKEN
};

enum 
{
	ICON_FILE,
	ICON_DIR,
	ICON_LINK,
	ICON_EXEC,
	ICON_SHARED,
	ICON_CORE,
	ICON_AUTHORS,
	ICON_MAKEFILE,
	ICON_NEWS,
	ICON_CHANGELOG,
	ICON_COPYING,
	ICON_INSTALL,
	ICON_README,
	ICON_TODO,
	ICON_NB
};

struct GC_file
{
	gchar *path;  /*path without basename*/
	gchar *name; 
	enum file_type type; /*file type*/
	GdkPixbuf *icon;
};

struct GC_icon
{
	gchar *ext;
	GdkPixbuf *icon;
};


/*used by g_slist_insert_sorted, you know what it do*/
gint file_cmp(gconstpointer a, gconstpointer b);

/*Return files and directories tree for path.
Sort is DIR_FIRST | ALPHA_SORT |INV_ALPHA_SORT*/
GSList *list_path(gchar *path, gint sort, struct GCommander *gcommander);

/*find icon*/
GdkPixbuf *find_pixbuf(GSList *icons, gchar *ext);

/*TRUE if ext is an image extension*/
gboolean *is_image(gchar *ext);

/*return icon pixbuf if found, else NULL*/
GdkPixbuf *icon_get_from_ext(struct GCommander *gcommander, gchar *file);

/*Try to get icon of a file not being reconizes with extension*/
GdkPixbuf *no_reconize_get_icon(gchar *filename, GdkPixbuf **icons, struct GCommander *gcommander);

/*load a pixbuf and scale it if we are in list view*/
GdkPixbuf *gdk_pixbuf_new_scaled_from_file(gchar *filepath, struct GCommander *gcommander);

#endif
