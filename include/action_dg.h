/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __ACTION_DG_H
#define __ACTION_DG_H

#include "gcommander.h"

#define ACTION_W 350
#define ACTION_H 200

enum files_actions
{
	PASTE_ACTION,
	DELETE_ACTION
};

enum
{
	Q_CHECK,
	Q_NAME,
	Q_COLUMNS
};

struct GCaction_dg
{
	GtkWidget *window;
	GtkWidget *view; /*file view*/
	GSList *delete_files; /*to delete files*/
	struct GC_paste *paste_files; /*to paste files*/
	struct GCommander *gcommander;
};

struct GC_paste
{
	GSList *files; /*files to paste*/
	gchar *to_path; /*where we want to paste*/
	gboolean cut;
};

/*Create an action dialog*/
void action_dg_new(struct GCommander *gcommander, enum files_actions action, gint button);

/*to delete files*/
GSList *get_to_delete_files(struct GCommander *gcommander);
	
/*to paste files*/
struct GC_paste *get_to_paste_files(struct GCommander *gcommander);
	
/*to delete files store*/
GtkTreeModel *get_to_delete_model(GSList *files);

/*to paste files store*/
GtkTreeModel *get_to_paste_model(struct GCaction_dg *action_dg);

/*add data to store*/
void file_append_foreach(gpointer data, gpointer store);

/*hide action window*/
void action_cancel(GtkWidget *widget, gpointer data);

/*delete action dialog callback*/
void action_dg_delete_callback(GtkWidget *widget, GdkEventButton *event, struct GCommander *gcommander);

/*delete action dialog callback*/
void action_dg_delete_menu_callback(GtkWidget *widget, struct GCommander *gcommander);

/*paste action dialog callback*/
void action_dg_paste_callback(GtkWidget *widget, GdkEventButton *event, struct GCommander *gcommander);

/*paste action dialog callback*/
void action_dg_paste_menu_callback(GtkWidget *widget, struct GCommander *gcommander);

/*callback for toggled button*/
void ch_toggled(GtkCellRendererToggle *cell, gchar *path_str, gpointer data);

/*remove name from files list*/
GSList *remove_from_list(GSList *files, gchar *name, gboolean basename);


#endif
