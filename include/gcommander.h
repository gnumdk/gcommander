/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __GCOMMANDER_H
#define __GCOMMANDER_H

#include <gnome.h>

#define APP_NAME "gcommander"
#define VERSION "0.1"
#define LG_PATH 1024
#define LG_BUFF 256
#define LG_EXT 128
#define WIDTH 600
#define HEIGHT 700
#define MAX_PROGS 10

#define ICONS_DIR "/home/gnumdk/Projects/Gcommander-0.1/pixmaps"
#define ICON_APP "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/gcommander-32x32.png"
#define LOGO_FILE "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/logo.png"
#define DEFAULT_DIR_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Dir.png"
#define DEFAULT_FILE_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/File.png"
#define DEFAULT_SYMLINK_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/link.png"
#define DEFAULT_EXEC_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Exec.png"
#define DEFAULT_SHARED_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Shared.png"
#define DEFAULT_CORE_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Core.png"
#define DEFAULT_AUTHORS_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Authors.png"
#define DEFAULT_MAKEFILE_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Makefile.png"
#define DEFAULT_NEWS_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/News.png"
#define DEFAULT_CHANGELOG_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Changelog.png"
#define DEFAULT_COPYING_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Copying.png"
#define DEFAULT_INSTALL_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Install.png"
#define DEFAULT_README_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Readme.png"
#define DEFAULT_TODO_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/Todo.png"
#define DEFAULT_UP_ICON "/home/gnumdk/Projects/Gcommander-0.1/pixmaps/up.png"

#define DEFAULT_GRP  _("Default")

#define TO_UTF8(name) g_filename_to_utf8(name, -1, NULL, NULL, NULL)
#define FROM_UTF8(name) g_filename_from_utf8(name, -1, NULL, NULL, NULL)

struct GC_statusbar
{
	GtkWidget *status_bar;
	gint context_id;
	gint nb_dirs;
	gint nb_files;
};


struct GC_edit
{
	/*copy,cut,paste*/
	GList *clipboard;
	GSList *to_paste;
	gchar *from_path; /*where user has used copy/cut*/
	gchar *selected; /*selected dir, if selected*/
	gboolean cut;
	gboolean lock_paste;
	gboolean action;
	/*delete*/
	GSList *to_delete;
};

struct GC_program
{
	gchar name[LG_BUFF]; /*#FIXME*/
	gchar cmd[LG_BUFF];
};

struct GC_mime
{
	gchar *name;
	gchar *group;
	gint nb_progs;
	struct GC_program progs[MAX_PROGS];
	GSList *exts;
	gchar *icon;
};

struct GC_group
{
	gchar *name;
	gint nb_progs;
	struct GC_program progs[MAX_PROGS];
	gchar *icon;	
	
};

struct GCommander
{
	/*widgets*/
	GtkWidget *main_window;
	GtkWidget *toolbar;
	GtkWidget *scrolled;
	GtkWidget *view;
	GtkWidget *location;
	GtkTreeModel *model;
	struct GC_statusbar gc_statusbar;
	/*history*/
	GSList *backward;
	GSList *forward;
	struct GC_edit edit;
	/*icons*/
	GSList *icon_list;
	/*mime*/
	GSList *mimes;
	GSList *groups;
	gboolean mime_is_show;
	/*useful info*/
	gchar current_path[LG_PATH];
	gboolean show_hiden;
	gboolean double_click;
	gboolean clear_icons;
	gchar *default_editor;
	/*use for file management*/
	GSList *files_list;
	gchar *tmp;
	gboolean quit;
};

#endif
