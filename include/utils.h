/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#ifndef __UTILS_H
#define __UTILS_H
#include "gcommander.h"

	
enum file_type_enum
{
	SHARED_ELF,
	CORE_ELF,
	EXEC_ELF,
	AUTHORS,
	MAKEFILE,
	NEWS,
	CHANGELOG,
	COPYING,
	INSTALL,
	README,
	TODO,
	UNKNOW
};

/*taken from gfilerunner project ;)*/
/*cursor busy*/
void set_cursor_busy(GtkWidget *window);
/*cursor normal*/
void set_cursor_normal(GtkWidget *window);

/*used by g_slist_insert_sorted, you know what it do*/
gint mime_cmp(gconstpointer a, gconstpointer b);

/*used by g_slist_insert_sorted, you know what it do*/
gint group_cmp(gconstpointer a, gconstpointer b);

/*return true if file is an ascii file, must work most time*/
gboolean is_ascii(gchar *filename);

/*lower a string
WARNING: Will not work with local specifique strings!*/
gchar *gchar_lower(gchar *to_lower);

#endif
